/*----- PROTECTED REGION ID(CILO.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        CILO.cpp
//
// description : C++ source for the CILO class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               CILO are implemented in this file.
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <CILO.h>
#include <CILOClass.h>

/*----- PROTECTED REGION END -----*/	//	CILO.cpp

/**
 *  CILO class description:
 *    Interlocking Logical Node
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  Health   |  Tango::DevShort	Scalar
//  Beh      |  Tango::DevShort	Scalar
//  Test     |  Tango::DevBoolean	Scalar
//  Blocked  |  Tango::DevBoolean	Scalar
//  EnaCls   |  Tango::DevBoolean	Scalar
//  EnaOpn   |  Tango::DevBoolean	Scalar
//================================================================

namespace CILO_ns
{
/*----- PROTECTED REGION ID(CILO::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	CILO::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : CILO::CILO()
 *	Description : Constructors for a Tango device
 *                implementing the classCILO
 */
//--------------------------------------------------------
CILO::CILO(Tango::DeviceClass *cl, string &s)
 : CommonLN(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(CILO::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	CILO::constructor_1
}
//--------------------------------------------------------
CILO::CILO(Tango::DeviceClass *cl, const char *s)
 : CommonLN(cl, s)
{
	/*----- PROTECTED REGION ID(CILO::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	CILO::constructor_2
}
//--------------------------------------------------------
CILO::CILO(Tango::DeviceClass *cl, const char *s, const char *d)
 : CommonLN(cl, s, d)
{
	/*----- PROTECTED REGION ID(CILO::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	CILO::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : CILO::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void CILO::delete_device()
{
	DEBUG_STREAM << "CILO::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(CILO::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	CILO::delete_device
	delete[] attr_EnaCls_read;
	delete[] attr_EnaOpn_read;

	if (Tango::Util::instance()->is_svr_shutting_down()==false  &&
		Tango::Util::instance()->is_device_restarting(device_name)==false &&
		Tango::Util::instance()->is_svr_starting()==false)
	{
		//	If not shutting down call delete device for inherited object
		CommonLN_ns::CommonLN::delete_device();
	}
}

//--------------------------------------------------------
/**
 *	Method      : CILO::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void CILO::init_device()
{
	DEBUG_STREAM << "CILO::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(CILO::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	CILO::init_device_before
	
	if (Tango::Util::instance()->is_svr_starting() == false  &&
		Tango::Util::instance()->is_device_restarting(device_name)==false)
	{
		//	If not starting up call init device for inherited object
		CommonLN_ns::CommonLN::init_device();
	}

	//	Get the device properties from database
	get_device_property();
	
	attr_EnaCls_read = new Tango::DevBoolean[1];
	attr_EnaOpn_read = new Tango::DevBoolean[1];
	/*----- PROTECTED REGION ID(CILO::init_device) ENABLED START -----*/
	
	//	Initialize device
    this->set_state(Tango::INIT);

	
	/*----- PROTECTED REGION END -----*/	//	CILO::init_device
}

//--------------------------------------------------------
/**
 *	Method      : CILO::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void CILO::get_device_property()
{
	/*----- PROTECTED REGION ID(CILO::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
	
	/*----- PROTECTED REGION END -----*/	//	CILO::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("LogicalDevice"));
	dev_prop.push_back(Tango::DbDatum("LogicalNode"));
	dev_prop.push_back(Tango::DbDatum("iecMapping"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on CILOClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		CILOClass	*ds_class =
			(static_cast<CILOClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize LogicalDevice from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  logicalDevice;
		else {
			//	Try to initialize LogicalDevice from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  logicalDevice;
		}
		//	And try to extract LogicalDevice value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  logicalDevice;

		//	Try to initialize LogicalNode from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  logicalNode;
		else {
			//	Try to initialize LogicalNode from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  logicalNode;
		}
		//	And try to extract LogicalNode value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  logicalNode;

		//	Try to initialize iecMapping from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  iecMapping;
		else {
			//	Try to initialize iecMapping from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  iecMapping;
		}
		//	And try to extract iecMapping value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  iecMapping;

	}

	/*----- PROTECTED REGION ID(CILO::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
    this->linkIECmapping_pointer();  // To be included in every LN!
	
	/*----- PROTECTED REGION END -----*/	//	CILO::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : CILO::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void CILO::always_executed_hook()
{
	DEBUG_STREAM << "CILO::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(CILO::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	CILO::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : CILO::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void CILO::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "CILO::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(CILO::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
    // Obligatory call on every LN:
    this->wait_for_read_attr_hardware_init();

    // Obligatory call on every LN:
    this->apply_common_read_attr_hardware();

	
	/*----- PROTECTED REGION END -----*/	//	CILO::read_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute EnaCls related method
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CILO::read_EnaCls(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CILO::read_EnaCls(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CILO::read_EnaCls) ENABLED START -----*/
	//	Set the attribute value

    if (dp_EnaCls_read == NULL){
            IecClient_ns::IecDataPoint idp;
            if(idp.define_IecDataPoint(this->get_mapped_dp(attr.get_name(),"ST"), this->get_name())){
                    dp_EnaCls_read = idp.findThisReferenceInVector(*this->listOfDataPoints);
                    if(dp_EnaCls_read == NULL){
                            dp_EnaCls_read = &FailbackIecDataPoint;
                    }
            }
            else
                    dp_EnaCls_read = &FailbackIecDataPoint;
    }

    attr.set_value_date_quality(dp_EnaCls_read->tangoDataPoint.valueBoolean, dp_EnaCls_read->tangoDataPoint.timestamp, dp_EnaCls_read->tangoDataPoint.quality);

//	attr.set_value(attr_EnaCls_read);
	
	/*----- PROTECTED REGION END -----*/	//	CILO::read_EnaCls
}
//--------------------------------------------------------
/**
 *	Read attribute EnaOpn related method
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CILO::read_EnaOpn(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CILO::read_EnaOpn(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CILO::read_EnaOpn) ENABLED START -----*/
	//	Set the attribute value

    if (dp_EnaOpn_read == NULL){
            IecClient_ns::IecDataPoint idp;
            if(idp.define_IecDataPoint(this->get_mapped_dp(attr.get_name(),"ST"), this->get_name())){
                    dp_EnaOpn_read = idp.findThisReferenceInVector(*this->listOfDataPoints);
                    if(dp_EnaOpn_read == NULL){
                            dp_EnaOpn_read = &FailbackIecDataPoint;
                    }
            }
            else
                    dp_EnaOpn_read = &FailbackIecDataPoint;
    }

    attr.set_value_date_quality(dp_EnaOpn_read->tangoDataPoint.valueBoolean, dp_EnaOpn_read->tangoDataPoint.timestamp, dp_EnaOpn_read->tangoDataPoint.quality);

//    attr.set_value(attr_EnaOpn_read);
	
	/*----- PROTECTED REGION END -----*/	//	CILO::read_EnaOpn
}

//--------------------------------------------------------
/**
 *	Method      : CILO::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void CILO::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(CILO::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	CILO::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : CILO::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void CILO::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(CILO::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	CILO::add_dynamic_commands
}

/*----- PROTECTED REGION ID(CILO::namespace_ending) ENABLED START -----*/

//	Additional Methods
// To be included in every LN!:
void CILO::linkIECmapping_pointer(){
        this->iECmapping_pointer = &iecMapping;
}


/*----- PROTECTED REGION END -----*/	//	CILO::namespace_ending
} //	namespace
