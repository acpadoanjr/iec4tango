#include "IecClientTask.h"
#include <string>
#include <libiec61850/mms_value.h>
#include <libiec61850/iec61850_client.h>

namespace IecClient_ns
{



        IecClientTask::IecClientTask(Tango::DeviceImpl * host_device):yat4tango::DeviceTask(host_device)
        {
                con = IedConnection_create();
                IedConnection_setConnectTimeout(con, 2000);
                this->set_timeout_msg_period(50000);
                this->enable_periodic_msg(false);
                this->enable_timeout_msg(false);
                CachedIecDataPoints = new yat::CachedAllocator<IecDataPoint, yat::Mutex>(10,100);

        }


        IecClientTask::~IecClientTask()
        {

                if (isConnectedf())
                {
                        INFO_STREAM << "closing connection in task destructor." << endl;
                        IedConnection_close(con);
                        IedConnection_destroy(con);
                }
                else
                        IedConnection_destroy(con);
                delete CachedIecDataPoints;



        }

        void IecClientTask::reportCallbackFunction(void* parameter, ClientReport report)
        {

                IecClientTask *task = (IecClientTask *)parameter;
                MmsValue *dataSetValues = ClientReport_getDataSetValues(report);
                vector<MmsValue *> listofMms;
                task->serializeMmsValue(listofMms, dataSetValues);
                listofMms.erase(listofMms.begin());
                struct timeval tp;
                gettimeofday(&tp, NULL);
                long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
                Timestamp ts;
                Timestamp_setTimeInMilliseconds(&ts, ms);
                int i = 0;
                int sizei = listofMms.size();
                do {
                        i = task->mapDS2DP(listofMms, &ts, report, i);
                } while (i<sizei && i!=-1);
        }

        /*
         * \bref Start several IEC 61850 services and check for sanity.
         */
        void IecClientTask::start_iec()
                throw (Tango::DevFailed)
        {
                DEBUG_STREAM << "start_iec called!" << endl;

                //Thread_sleep(20);
                this->enable_timeout_msg(true);

                if(!start_iecConnection())
                {
                        ERROR_STREAM << "Error while creating IEC connector. NO IED on network?" << endl;
                        yat::Message *msgtime = new yat::Message(yat::TASK_TIMEOUT);
                        this->post(msgtime);
                        THROW_DEVFAILED(
                                _CPTC("OPERATION_NOT_ALLOWED"),
                                _CPTC("IED is not present..."),
                                _CPTC("IecClientTask::start_iec"));
                }
                if(!start_iecModel())
                {
                        ERROR_STREAM << "Error while reading Model." << endl;
                        yat::Message *msgtime = new yat::Message(yat::TASK_TIMEOUT);
                        this->post(msgtime);
                        THROW_DEVFAILED(
                                _CPTC("OPERATION_NOT_ALLOWED"),
                                _CPTC("IEC model not existant..."),
                                _CPTC("IecClientTask::start_iec"));
                }
                if(!start_iecReport()){
                        ERROR_STREAM << "Error while reading Report." << endl;
                        yat::Message *msgtime = new yat::Message(yat::TASK_TIMEOUT);
                        this->post(msgtime);
                        THROW_DEVFAILED(
                                _CPTC("OPERATION_NOT_ALLOWED"),
                                _CPTC("Report does not respond... name invalid?"),
                                _CPTC("IecClientTask::start_iec"));

                }
                if(!start_iecDataSet())
                {
                        ERROR_STREAM << "Error while reading DataSet." << endl;
                        yat::Message *msgtime = new yat::Message(yat::TASK_TIMEOUT);
                        this->post(msgtime);
                        THROW_DEVFAILED(
                                _CPTC("OPERATION_NOT_ALLOWED"),
                                _CPTC("DataSet not configured or not existant..."),
                                _CPTC("IecClientTask::start_iec"));
                }
                if(!startControls())
                {
                        ERROR_STREAM << "Error while starting Control Blocks." << endl;
                        yat::Message *msgtime = new yat::Message(yat::TASK_TIMEOUT);
                        this->post(msgtime);
                        THROW_DEVFAILED(
                                _CPTC("OPERATION_NOT_ALLOWED"),
                                _CPTC("Control Block not configured or not existant..."),
                                _CPTC("IecClientTask::start_iec"));
                }
                try
                {
                        /* prepare the parameters of the RCP */
                        string dstest(
                                ClientReportControlBlock_getDataSetReference(*rcbs[0]));
                        INFO_STREAM << "reporting handling DS (test): " << dstest << std::endl;
                        ClientReportControlBlock_setResv(*rcbs[0], true);
                        ClientReportControlBlock_setRptEna(*rcbs[0], true);
                        ClientReportControlBlock_setGI(*rcbs[0], false);
                        ClientReportControlBlock_setIntgPd(*rcbs[0],15000);

                        /* Configure the report receiver */
                        regex reg_rep("^(\\w+/\\w+\\.\\w+(?:\\.[\\w|\\.]*)?\\D)(\\d+)$");
                        smatch match_rep;
                        if (regex_search(reportName1, match_rep, reg_rep) == true){
                                string r(match_rep.str(1));
                                INFO_STREAM << "reporting handled: " << r << std::endl;
                                IedConnection_installReportHandler(con, r.c_str(),
                                                                   ClientReportControlBlock_getRptId(*rcbs[0]),
                                                                   &IecClientTask::reportCallbackFunction,
                                                                   (void*) this);

                        } else {
                                ERROR_STREAM << "reporting name error: " << match_rep.str(1) << endl;
                                this->closeConnection();
                                THROW_DEVFAILED(
                                        _CPTC("OPERATION_NOT_ALLOWED"),
                                        _CPTC("Report name not standard."),
                                        _CPTC("IecClientTask::start_iec"));
                        }

                        /* Write RCB parameters and enable report */
                        INFO_STREAM << "Trying to enable report." << std::endl;
                        Thread_sleep(100);
                        IedConnection_setRCBValues(con, &clerror, *rcbs[0], RCB_ELEMENT_RESV | RCB_ELEMENT_DATSET | RCB_ELEMENT_RPT_ENA | RCB_ELEMENT_GI | RCB_ELEMENT_INTG_PD, true);

                        if (clerror != IED_ERROR_OK) {
                                ERROR_STREAM << "setRCBValues service error!" << endl;
                                this->closeConnection();
                                THROW_DEVFAILED(
                                        _CPTC("OPERATION_NOT_ALLOWED"),
                                        _CPTC("setRCBValues do not exist..."),
                                        _CPTC("IecClientTask::start_iec"));

                        }
                        //DEBUG_STREAM << "Trying a GI on report." <<  std::endl;
                        //Thread_sleep(100);

                        /* Trigger GI Report */
                        ClientReportControlBlock_setGI(*rcbs[0], true);
                        IedConnection_setRCBValues(con, &clerror, *rcbs[0], RCB_ELEMENT_GI, true);

                        if (clerror != IED_ERROR_OK) {
                                ERROR_STREAM << "Error triggering a GI report (code:" << clerror << ")" << endl;
                                this->closeConnection();
                                THROW_DEVFAILED(
                                        _CPTC("OPERATION_NOT_ALLOWED"),
                                        _CPTC("GI report not working..."),
                                        _CPTC("IecClientTask::start_iec"));
                        }

                }
                catch(Tango::DevFailed e)
                {
                        ERROR_STREAM << "Error unknwon with report" << endl;
                        ClientReportControlBlock_setRptEna(*rcbs[0], false);
                        IedConnection_setRCBValues(con, &clerror, *rcbs[0], RCB_ELEMENT_RPT_ENA, true);
                        THROW_DEVFAILED(
                                _CPTC("OPERATION_NOT_ALLOWED"),
                                _CPTC("Report not working"),
                                _CPTC("IecClientTask::start_iec"));

                }
                Thread_sleep(500);
                if(!ClientReportControlBlock_getRptEna(*rcbs[0])){
                  INFO_STREAM << "Enable report failed!" << std::endl;
                } else {
                  INFO_STREAM << "Report enabled!" << std::endl;
                }
                isConnectedf();
        }





        void IecClientTask::exit ()
                throw (Tango::DevFailed)
        {
                /* disable reporting */
                try
                {
                        DEBUG_STREAM << "Closing connection." << endl;
                        this->closeConnection();
                        if(CachedIecDataPoints->length()>0)
                                CachedIecDataPoints->clear();
                        //this->enable_timeout_msg(false);


                }
                catch(...)
                {
                        ERROR_STREAM << "Error stopping the connection, trying again" << endl;
                        try
                        {

                                this->closeConnection();

                        }
                        catch(...)
                        {
                                THROW_DEVFAILED(
                                        _CPTC("OPERATION_NOT_ALLOWED"),
                                        _CPTC("problem closing iec connection..."),
                                        _CPTC("IecClientTask::exit"));
                        }
                }

        }

        void IecClientTask::process_message (yat::Message& msg)
        {
                //DEBUG_STREAM << "IecClientTask::handle_message..." << std::endl;
                switch (msg.type())
                {
                        //- THREAD_INIT ----------------------
                case yat::TASK_INIT:
                {
                        DEBUG_STREAM << "IecClientTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;

                        yat::Message *msg = new yat::Message(INIT_MSG_TYPE);
                        this->post(msg, 5000);

                }
                break;
                case yat::TASK_TIMEOUT:
                  {
                    try
                      {
                        if (isConnectedf())
                          {
                            INFO_STREAM << "closing connection after timeout" << endl;
                            this->closeConnection();
                          }
                        else
                        {
                                if (IedConnection_getState(con) == IED_STATE_CLOSED){
                                        this->closeConnection();
                                }
                                this->invalidate();
                                INFO_STREAM << "trying connection after timeout" << endl;
                                this->start_iec();
                        }
                      }
                    catch(...)
                      {
                        THROW_DEVFAILED(
                                        _CPTC("OPERATION_NOT_ALLOWED"),
                                        _CPTC("problem closing iec connection after timeout..."),
                                        _CPTC("IecClientTask::process_message"));
                      }

                  }
                  break;
                case yat::FIRST_USER_MSG:
                {
                        DEBUG_STREAM << "Report received" << endl;
                        IecDataPoint *midp = msg.get_data<IecDataPoint*>();
                        IecDataPoint *lidp = midp->findThisReferenceInVector(mappedDataPoints,false);
                        if (lidp != NULL){
                                if (!lidp->hasValue())
                                        lidp->set_iecValue(MmsValue_clone(midp->get_iecValue()));
                                else
                                        MmsValue_update(lidp->get_iecValue(), midp->get_iecValue());
                                lidp->set_iecQ(midp->get_iecQ());
                                lidp->set_iecT(midp->get_iecT());
                                lidp->convert();
                                CachedIecDataPoints->free(midp);
                        }
                }
                break;
                case OPER_MSG_TYPE:
                {
                        IecDataPoint *midp = msg.get_data<IecDataPoint*>();
                        midp->convert();
                        if (ControlObjectClient_operate(midp->controlObj, midp->get_iecCtlVal(), 0 /* operate now */)) {
                                INFO_STREAM << "Oper sent and returned succes" << endl;

                        }
                        else {
                                INFO_STREAM << "Oper sent and returned fail" << endl;
                        }

                }
                break;
                case POLL_MSG_TYPE:
                {
                        IecDataPoint* midp = msg.get_data<IecDataPoint*>();
                        if(read_mmsValue(midp)){
                                INFO_STREAM << "Polling returned succes" << endl;
                        }
                        else {
                                INFO_STREAM << "Polling returned failed" << endl;
                        }
                        CachedIecDataPoints->free(midp);

                }
                break;
                case INIT_MSG_TYPE:
                {
                        try
                        {
                                this->start_iec();
                        }
                        catch(...)
                        {
                                INFO_STREAM << "Start connection failed." << endl;

                                if (isConnectedf())
                                {
                                        INFO_STREAM << "closing connection after bad start." << endl;
                                        this->closeConnection();

                                }
                        }

                }
                break;
                default:
                        DEBUG_STREAM << "IecClientTask::handle_message::unhandled msg type received: " << msg.type() << endl;
                        break;
                }

        }

        bool IecClientTask::start_iecConnection()
        {
                if (isConnectedf())
                        return true;
                IedConnection_connect(con, &clerror, ipaddress.c_str(), (int) port);
                if (clerror == IED_ERROR_OK)
                {
                        INFO_STREAM << " Connection established." << endl;
                        return true;
                }
                else
                {
                        INFO_STREAM << "Connection not possible. IED is not present on network!" << endl;
                        return false;
                }
        }



///////////////////////////////////////////////////////////////////////////////
//      Get dynamically the variables list in DS and in order(from IED)      //
///////////////////////////////////////////////////////////////////////////////
        bool IecClientTask::fill_list_of_DataSet(vector<iecVariable> &list_to_be_filled, LinkedList DataDirectory) {
                INFO_STREAM << "Geting DataSet information" << endl;
                int i;
                MmsVariableSpecification *vspec;
                IecDataPoint idp;
                for (i = 0; i < LinkedList_size(DataDirectory); i++){
                        LinkedList entry = LinkedList_get(DataDirectory, i);
                        string entryName((char*) entry->data);
                        DEBUG_STREAM << "Looking in " << entryName << endl;
                        if(!idp.define_IecDataPoint(entryName)){
                                ERROR_STREAM << "Name of object in Data Directory list not in good format." << endl;
                                return false;
                        }
                        string add = idp.get_iecAddress();
                        iecVariable variDO;
                        vspec = get_DO_varSpec_by_FC(entryName);
                        if (vspec != NULL) {
                                variDO.da_name = add;
                                variDO.FC = idp.get_FC_string();
                                variDO.parent_name = "";
                                variDO.type = MmsVariableSpecification_getType(vspec);
                                variDO.var_spec = vspec;
                                variDO.index_from_parent = -1;
                                variDO.isStructure = true;
                                variDO.structureSize = vspec->typeSpec.structure.elementCount;
                                variDO.isDO = true;
                                variDO.dataSetDirectoryIndex = i;
                                // printf("varSpec DO: %s, type: %i, fc: %s \n",
                                //         variDO.da_name.c_str(),
                                //         (int) variDO.type,
                                //         variDO.FC.c_str());
                                 list_to_be_filled.push_back(variDO);

                        }
                        else{
                                ERROR_STREAM << "Error in getting  DO variable specification." << endl;
                                return false;
                        }
                        fillListOfVariables(list_to_be_filled,
                                            add.c_str(),
                                            idp.get_FC(),
                                            vspec);
                }
                return true;
        }

///////////////////////////////////////////////////////////////////////////////
//                          check t and q in DS list                         //
///////////////////////////////////////////////////////////////////////////////
        void IecClientTask::organize_q_t_in_list(vector<iecVariable> &list_to_be_org)
        {
                INFO_STREAM << "Looking for q and t" << endl;

                iecVariable* DOloop = NULL;
                int k = 0;
                int lastDOIndex = -1;
                //pushing quality to parent
                for (std::vector<iecVariable>::iterator it=list_to_be_org.begin();
                     it != list_to_be_org.end(); ++it){
                        DOloop = &(*it);

                        INFO_STREAM << "Looking at " << DOloop->da_name << endl;

                        if (DOloop->isQuality){
                                list_to_be_org[DOloop->index_from_parent].QualityIndex = k;
                                list_to_be_org[DOloop->index_from_parent].hasQuality = true;

                        }
                        if (DOloop->isTimestamp){
                                list_to_be_org[DOloop->index_from_parent].TimestampIndex = k;

                        }
                        if (DOloop->isDO){
                                if (lastDOIndex != -1)
                                        list_to_be_org[lastDOIndex].nextDOIndex = k;
                                lastDOIndex = k;
                        }
                        k++;

                }
                //pushing quality to children (structure and array)
                for (std::vector<iecVariable>::iterator it=list_to_be_org.begin();
                     it != list_to_be_org.end(); ++it){
                        DOloop = &(*it);

                        if (!DOloop->isDO && (list_to_be_org[DOloop->index_from_parent].hasQuality == true) && (DOloop->isStructure || DOloop->isArray)){
                                DOloop->hasQuality = true;
                                DOloop->QualityIndex = list_to_be_org[DOloop->index_from_parent].QualityIndex;
                                DOloop->TimestampIndex = list_to_be_org[DOloop->index_from_parent].TimestampIndex;
                        }

                }



                for (std::vector<iecVariable>::iterator it=list_to_be_org.begin();
                     it != list_to_be_org.end(); ++it){
                        DOloop = &(*it);
                        string address(DOloop->da_name);
                        address.append("[");
                        address.append(DOloop->FC);
                        address.append("]");
                        IecDataPoint idp2;
                        if(idp2.define_IecDataPoint(address)){
                                DOloop->associatedDataPoint = idp2.findThisReferenceInVector(this->mappedDataPoints,false);
                                if (DOloop->associatedDataPoint != NULL){
                                        DOloop->isMapped = true;
                                        DOloop->associatedDataPoint->Report = true;
                                }
                        }

                        /*
                        printf("da_name: %s, qi: %i, ti: %i, ndo: %i, is mapped %s, has Q %s, FC: %s, type %i, parentindex %i \n",
                               DOloop->da_name.c_str(),
                               DOloop->QualityIndex,
                               DOloop->TimestampIndex,
                               DOloop->nextDOIndex,
                               DOloop->isMapped ? "true" : "false",
                               DOloop->hasQuality ? "true" : "false",
                               DOloop->FC.c_str(),
                               DOloop->type,
                               DOloop->index_from_parent);
                        */

                }

                INFO_STREAM << "DataSet second analyses finished." << endl;
        }

        bool IecClientTask::start_iecModel()
        {
                regex reg_fc("^\\w+\\[(\\w\\w)\\]$");
                smatch match_fc;

                INFO_STREAM << "IED Model will be request." << endl;
                Thread_sleep(500);
                LinkedList DataDirectory;
                try {
                        DataDirectory = IedConnection_getServerDirectory(con, &clerror, false);
                }
                catch(...){
                        ERROR_STREAM << "IED Model reading failed. Server strange behaviour." << endl;
                        return false;
                }
                INFO_STREAM << "IED Model received. parsing..." << endl;
                if (clerror != IED_ERROR_OK) {
                        ERROR_STREAM << "IED Model reading failed." << endl;
                        return false;
                }
                if (DataDirectory == NULL || LinkedList_size(DataDirectory) == 0) {
                        ERROR_STREAM << "Returning IED Model is null or empty." << endl;
                        return false;
                }
                for (int i = 0; i < LinkedList_size(DataDirectory); i++){
                        LinkedList entry = LinkedList_get(DataDirectory, i);
                        string entryName((char*) entry->data);
                        listLogicalDevices.push_back(entryName);
                        INFO_STREAM << "Logical Device : " << entryName << endl;
                }
                LinkedList_destroy(DataDirectory);
                for (std::vector<string>::iterator it=listLogicalDevices.begin();
                     it != listLogicalDevices.end(); ++it){
                        string LD = *it;
                        DataDirectory = IedConnection_getLogicalDeviceDirectory(con, &clerror, LD.c_str());
                        if (clerror != IED_ERROR_OK) {
                                ERROR_STREAM << "Logical Node reading failed." << endl;
                                return false;
                        }
                        if (DataDirectory == NULL || LinkedList_size(DataDirectory) == 0) {
                                ERROR_STREAM << "Returning LN is null or empty." << endl;
                                return false;
                        }
                        for (int i = 0; i < LinkedList_size(DataDirectory); i++){
                                LinkedList entry = LinkedList_get(DataDirectory, i);
                                string entryName((char*) entry->data);
                                string LN(LD + "/" + entryName);
                                if(sizeof(entryName) > 0){
                                        listLogicalNodes.push_back(LN);
                                        INFO_STREAM << "Logical Node : " << LN << endl;
                                }
                        }
                }

                INFO_STREAM << "Reading data objects..." << endl;
                LinkedList_destroy(DataDirectory);
                for (std::vector<string>::iterator it=listLogicalNodes.begin();
                     it != listLogicalNodes.end(); ++it){
                        string LN = *it;
                        DEBUG_STREAM << "Reading Data objects for" << LN << endl;
                        try{
                                DataDirectory = IedConnection_getLogicalNodeDirectory(con,&clerror,
                                                                                      LN.c_str(),
                                                                                      ACSI_CLASS_DATA_OBJECT);
                        }catch(...){
                                ERROR_STREAM << "DataObject reading failed." << endl;
                                return false;
                        }
                        if (clerror != IED_ERROR_OK) {
                                ERROR_STREAM << "DataObject reading failed." << endl;
                                return false;
                        }
                        if (DataDirectory == NULL || LinkedList_size(DataDirectory) == 0) {
                                ERROR_STREAM << "Returning DO Model is null or empty." << endl;
                                return false;
                        }
                        for (int i = 0; i < LinkedList_size(DataDirectory); i++){
                                LinkedList entry = LinkedList_get(DataDirectory, i);
                                string entryName((char*) entry->data);
                                string DOb(LN + "." + entryName);
                                // Reading FCs ------------------------------------------------
                                LinkedList DataAttDirectory;
                                try{
                                        DataAttDirectory = IedConnection_getDataDirectoryFC(con,
                                                                                            &clerror,
                                                                                            DOb.c_str());
                                }catch(...){
                                        ERROR_STREAM << "FC reading failed." << endl;
                                        return false;
                                }
                                if (clerror != IED_ERROR_OK) {
                                        ERROR_STREAM << "FC reading failed." << endl;
                                        return false;
                                }

                                std::vector<string> FCs;
                                for (int j = 0; j < LinkedList_size(DataAttDirectory); j++){
                                        LinkedList entry2 = LinkedList_get(DataAttDirectory, j);
                                        string entryName2((char*) entry2->data);
                                        //cout << "Data Attribute : " << entryName2 << endl;
                                        if (regex_search(entryName2, match_fc, reg_fc) == true){
                                                string fc(match_fc.str(1));
                                                if (std::find(FCs.begin(), FCs.end(), fc) == FCs.end()) {
                                                        FCs.push_back(fc);
                                                }
                                        }
                                }

                                for (std::vector<string>::iterator it=FCs.begin(); it != FCs.end(); ++it) {
                                        string FC = *it;
                                        string DObFC = DOb + "[" + FC + "]";
                                        listDataObjects.push_back(DObFC);
                                        //cout << "Data Object : " << DObFC << endl;
                                }

                        }
                        INFO_STREAM << "DOs read for " << LN << endl;
                }

                INFO_STREAM << "All DOs read." << endl;

                LinkedList_destroy(DataDirectory);
                DataDirectory = LinkedList_create();
                for (std::vector<string>::iterator it=listDataObjects.begin();
                     it != listDataObjects.end(); ++it){
                        string DOb = *it;
                        char * cstr = new char [DOb.length()+1];
                        std::strcpy (cstr, DOb.c_str());
                        LinkedList_add(DataDirectory, cstr);
                }
                if (DataDirectory == NULL || LinkedList_size(DataDirectory) == 0) {
                        ERROR_STREAM << "Returning list of DObjs is null or empty." << endl;
                        return false;
                }
                if(!fill_list_of_DataSet(listAllVariables,DataDirectory)){
                        LinkedList_destroy(DataDirectory);
                        return false;
                }
                LinkedList_destroy(DataDirectory);
                return true;
        }

        bool IecClientTask::start_iecReport()
        {
                INFO_STREAM << "---------------Reports will be read." << endl;
          // int RepIndex = 0;
          std::vector<string> reps = unBufRepNames;
          for (std::vector<string>::iterator it = reps.begin();
               it != reps.end(); ++it) {
            /* Read RCB values */
            string repName = *it;
            INFO_STREAM << "---------------Report will be read: " << repName
                        << "." << endl;
            regex reg_rep("^(\\w+/\\w+\\.\\w+(?:\\.[\\w|\\.]*)?\\D)(\\d+)$");
            smatch match_rep;
            if (regex_search(repName, match_rep, reg_rep) == true) {
              string r(match_rep.str(1));
              INFO_STREAM << "reporting has good format: " << r << std::endl;
              ClientReportControlBlock *rcb =
                  new ClientReportControlBlock(IedConnection_getRCBValues(
                      con, &clerror, repName.c_str(), NULL));
              if (clerror != IED_ERROR_OK) {
                ERROR_STREAM << "getRCBValues read service error!" << endl;
                return false;
              }

              try {
                if (ClientReportControlBlock_getRptEna(*rcb)) {
                  ClientReportControlBlock_setRptEna(*rcb, false);
                  IedConnection_setRCBValues(
                      con, &clerror, *rcbs[0],
                      RCB_ELEMENT_RESV | RCB_ELEMENT_DATSET |
                          RCB_ELEMENT_RPT_ENA | RCB_ELEMENT_GI,
                      true);
                }
              } catch (...) {
                ERROR_STREAM << "Report already used by another client?"
                             << endl;
                return false;
              }
              if (clerror != IED_ERROR_OK) {
                ERROR_STREAM << "Report already used by another client?!"
                             << endl;
                return false;
              }

              string s(ClientReportControlBlock_getDataSetReference(*rcb));
              if (s.empty() || sizeof(s) == 0) {
                ERROR_STREAM << "No dataset configured in report!" << endl;
                return false;
              }

              std::replace(s.begin(), s.end(), '$',
                           '.'); ///* NOTE the "." instead of "$" ! */
              dataSetNames.push_back(s);
              rcbs.push_back(rcb);
              dataSetName1 = s;

              INFO_STREAM << "---------------DataSet found : " << s
                          << "." << endl;
            } else {
              ERROR_STREAM << "reporting name error: " << match_rep.str(1)
                           << endl;
              return false;
            }
                }
                return true;
        }


        bool IecClientTask::start_iecDataSet()
        {
                INFO_STREAM << "---------------DataSet will be read: " << dataSetName1 << "." << endl;


                dataSetDirectory = IedConnection_getDataSetDirectory(con, &clerror, dataSetName1.c_str(), NULL);
                INFO_STREAM << "Reading data set..." << endl;
                if (clerror != IED_ERROR_OK) {
                        ERROR_STREAM << "DataSet reading failed. Check DataSet Name." << endl;
                        return false;
                }
                INFO_STREAM << "Reading data set inside ..." << endl;

                clientDataSet = IedConnection_readDataSetValues(con, &clerror, dataSetName1.c_str(), NULL);
                if (clerror != IED_ERROR_OK || clientDataSet == NULL) {
                        ERROR_STREAM << "DataSet reading NULL. Check DataSet Configuration." << endl;
                        return false;
                }
                if (!fill_list_of_DataSet(listDataSetVariable,dataSetDirectory)){
                        ERROR_STREAM << "DataSet could not be parsed" << endl;
                        return false;
                }
                INFO_STREAM << "Parsing DataSet variables finished." << endl;
                organize_q_t_in_list(listDataSetVariable);
                return true;
        }


///////////////////////////////////////////////////////////////////////////////
//       After reading DS list, get all attributes in order(IED order)       //
///////////////////////////////////////////////////////////////////////////////
        void IecClientTask::fillListOfVariables(vector<iecVariable> &list_da, const char* directory_name, FunctionalConstraint FC, MmsVariableSpecification* parent_var_spec)
        {
                int j;
                int index_from_parent = list_da.size()-1;
                MmsVariableSpecification *vspec;
                for (j=0; j < parent_var_spec->typeSpec.structure.elementCount ; j++){
                        string addda(directory_name);

                        vspec = parent_var_spec->typeSpec.structure.elements[j];


                        iecVariable var;
                        string name(MmsVariableSpecification_getName(vspec));
                        if (name.compare("q")==0){
                                var.isQuality = true;
                        }
                        if (name.compare("t")==0){
                                var.isTimestamp = true;
                        }

                        var.da_name = addda.append(".").append(MmsVariableSpecification_getName(vspec));
                        string dobj(directory_name);
                        var.FC = FunctionalConstraint_toString(FC);
                        var.parent_name = dobj;
                        var.type = MmsVariableSpecification_getType(vspec);
                        var.var_spec = vspec;
                        var.index_from_parent=index_from_parent;


                        if (((int) MmsVariableSpecification_getType(vspec)) > 1) {
                                var.isVar = true;
                                list_da.push_back(var);
                                 // printf("varSpec: %s, add: %s  type: %i do: %s inx: %i  \n",
                                 //        MmsVariableSpecification_getName(vspec), var.da_name.c_str(),
                                 //        (int) MmsVariableSpecification_getType(vspec),
                                 //        dobj.c_str(), var.index_from_parent);
                        }
                        else {
                                 // printf("varSpec: %s, add: %s  type: %i do: %s inx %i  qual: %s \n",
                                 //        MmsVariableSpecification_getName(vspec), addda.c_str(),
                                 //        (int) MmsVariableSpecification_getType(vspec), dobj.c_str(),
                                 //        var.index_from_parent, var.hasQuality ? "true" : "false");
                                var.structureSize = vspec->typeSpec.structure.elementCount;

                                if(((int) MmsVariableSpecification_getType(vspec))==1){
                                        var.isStructure = true;
                                }
                                else
                                        var.isArray = true;
                                list_da.push_back(var);

                                iecVariable lastDO = list_da.back();
                                IecClientTask::fillListOfVariables(list_da,
                                                                   addda.c_str() , FC, vspec);
                        }
                }

        }

///////////////////////////////////////////////////////////////////////////////
//      scan report and send changed values for treatment in thread task     //
///////////////////////////////////////////////////////////////////////////////
        int IecClientTask::mapDS2DP(vector<MmsValue*> list_mms, Timestamp* stdTimestamp, ClientReport rep, int i=0)
        {
                MmsValue* currentmms = list_mms[i];
                iecVariable currentds = listDataSetVariable[i];
                if(currentds.isDO)
                {
                        int j = currentds.dataSetDirectoryIndex;
                        ReasonForInclusion reason = ClientReport_getReasonForInclusion(rep, j);
                        if (reason == IEC61850_REASON_NOT_INCLUDED) {
                                return currentds.nextDOIndex;
                        }

                }
                if (currentds.isStructure || currentds.isArray || currentds.isQuality || currentds.isTimestamp || !currentds.isMapped)
                {
                        return ++i;
                }

                Timestamp *t = stdTimestamp;
                Quality q = 0;
                iecVariable parent = listDataSetVariable[currentds.index_from_parent];
                //INFO_STREAM << "Posting report " << currentds.da_name  << endl;

                if (parent.hasQuality){
                        t = Timestamp_createFromByteArray(MmsValue_getUtcTimeBuffer(list_mms[parent.TimestampIndex]));
                        q = Quality_fromMmsValue(list_mms[parent.QualityIndex]);
                }
                IecDataPoint *idp;
                idp = CachedIecDataPoints->malloc();
                string address(currentds.da_name);
                address.append("[");
                address.append(currentds.FC);
                address.append("]");
                idp->define_IecDataPoint(address);
                idp->set_iecValue(MmsValue_clone(currentmms));
                idp->set_iecQ(q);
                idp->set_iecT(*t);
                yat::Message *msg = new yat::Message(yat::FIRST_USER_MSG);
                msg->attach_data<IecDataPoint*>(idp);
                this->post(msg);
                return ++i;

        }

        IecDataPoint* IecClientTask::GetCachedIecDataPoint(){
                return CachedIecDataPoints->malloc();
        }

        void IecClientTask::serializeMmsValue(vector<MmsValue*> &list_mms, MmsValue* mmsvalue)
        {

                if (MmsValue_getType(mmsvalue) > 1){
                        //DEBUG_STREAM << "MMS value is basic" << endl;
                        list_mms.push_back(mmsvalue);
                }
                else
                {
                        //DEBUG_STREAM << "MMS value is struct or array" << endl;
                        list_mms.push_back(mmsvalue);
                        int mmssize = MmsValue_getArraySize(mmsvalue);
                        for(int k=0; k<mmssize;k++){
                                serializeMmsValue(list_mms, MmsValue_getElement(mmsvalue,k));
                        }
                }
        }

        void IecClientTask::set_tg(Tango::Util *tangoUtil)
        {
                this->tg = tangoUtil;
        }

        void IecClientTask::set_mappedDataPoints(vector<IecDataPoint*> vectorOfMappedDP)
        {
                this->mappedDataPoints = vectorOfMappedDP;
        }

        bool IecClientTask::isConnectedf(){

                IedConnectionState state;
                try{
                        state = IedConnection_getState(con);
                }catch(...){
                        this->isConnected = false;
                        return false;
                }

                if (state == IED_STATE_CONNECTED)
                        this->isConnected = true;
                else
                        this->isConnected = false;
                DEBUG_STREAM << "Checking connection.." << this->isConnected << endl;
                return this->isConnected;
        }

        void IecClientTask::set_IPAddress(string IEDaddress, Tango::DevShort IEDport){
                ipaddress = IEDaddress;
                port = IEDport;
        }

        void IecClientTask::set_ReportName(string ReportName1, string ReportName2){
                reportName1 = ReportName1;
                reportName2 = ReportName2;
        }

        void IecClientTask::set_ReportNames(std::vector<string> iBufRepNames, std::vector<string> iUnBufRepNames){
                bufRepNames = iBufRepNames;
                unBufRepNames = iUnBufRepNames;

        }

        void IecClientTask::closeConnection(){
                if(isConnectedf())
                        IedConnection_close(con);
                if(!isConnectedf()){
                        if (clientDataSet != NULL)
                        {
                                ClientDataSet_destroy(clientDataSet);
                        }
                        if (*rcbs[0] != NULL) {
                                ClientReportControlBlock_destroy(*rcbs[0]);
                        }
                        rcbs.clear();
                        listDataSetVariable.clear();
                        this->invalidate();
                }
        }


        void IecClientTask::invalidate(){
                INFO_STREAM << "Forcing attributes to invalid." << endl;
                int i=0;
                IecDataPoint *idp;
                for (vector<IecDataPoint*>::iterator it = mappedDataPoints.begin(); it != mappedDataPoints.end(); ++it) {
                        i++;
                        idp = (*it);
                        idp->invalidate();
                }
        }

        bool IecClientTask::startControls(){
                IecDataPoint *idp;
                for (vector<IecDataPoint*>::iterator it = mappedDataPoints.begin(); it != mappedDataPoints.end(); ++it) {
                        idp = (*it);
                        string FC(idp->get_FC_string());
                        if(FC.compare("CO") == 0){
                                string DOcontr(idp->get_DOAddress());
                                //cout << DOcontr << endl;
                                idp->controlObj = ControlObjectClient_create(DOcontr.c_str(), this->con);
                                //cout << idp->controlObj << endl;
                                ControlObjectClient_setOrigin(idp->controlObj, "Tango", 2);
                                //cout << DOcontr << endl;
                                // TODO: understand why the "if" below is true:
                                //if(idp->controlObj == NULL);
                                //        return false;
                                //idp->iecDataPoint.Quality = 0;
                        }

                }
                return true;
        }

        MmsVariableSpecification* IecClientTask::get_DO_varSpec_by_FC(string DOnameWithFC){
                MmsVariableSpecification *vspec;
                IecDataPoint idp;
                if(!idp.define_IecDataPoint(DOnameWithFC)){
                        ERROR_STREAM << "Name of object not in good format." << endl;
                        return NULL;
                }
                string add = idp.get_iecAddress();
                vspec = IedConnection_getVariableSpecification(con,&clerror, add.c_str(), idp.get_FC());
                if (clerror != IED_ERROR_OK) {
                        ERROR_STREAM << "reading DO directory name failed. Check IED services." << endl;
                        this->exit();

                        // TODO Proble here!!
                        return NULL;
                }

                return vspec;
        }

///////////////////////////////////////////////////////////////////////////////
//                           Slow MMS read function                          //
///////////////////////////////////////////////////////////////////////////////
        bool IecClientTask::read_mmsValue(IecDataPoint *idp){
                string addr(idp->get_iecAddress(false));
                string addrDO = idp->get_DOAddress(true);
                IedClientError ierror;
                DEBUG_STREAM << "Polling DO:" << addrDO << endl;
                MmsValue* Val = NULL;
                try{
                        Val = IedConnection_readObject(con, &ierror, idp->get_DOAddress(false).c_str(), idp->get_FC());
                }catch(...){
                        DEBUG_STREAM << "Polling Read Object failed:" << addrDO << endl;
                        return false;
                }

                if (ierror != IED_ERROR_OK) {
                        ERROR_STREAM << "Polling MMS Value failed" << endl;
                        return false;
                }

                vector<MmsValue*> list_mms;
                serializeMmsValue(list_mms, Val);


                //idp->set_iecValue(Val);  // TODO: check if this works

                char* addrDOc = (char*) addrDO.c_str();
                std::vector<iecVariable>  mlistOfDAs;
                LinkedList DObj = LinkedList_create();
                LinkedList_add(DObj, addrDOc);
                if(fill_list_of_DataSet(mlistOfDAs, DObj))
                        organize_q_t_in_list(mlistOfDAs); // TODO: To check q et t...   TBC
                else{
                        ERROR_STREAM << "Polling MMS Value q and t failed" << endl;

                }
                struct timeval tp;
                gettimeofday(&tp, NULL);
                long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
                Timestamp ts;
                Timestamp* pts = NULL;
                Timestamp_setTimeInMilliseconds(&ts, ms);

                Quality q = 0;
                iecVariable parent = mlistOfDAs[0];

                ////  TODO : Find good pointer usage for Val in expressions below
                if (parent.hasQuality){
                        DEBUG_STREAM << "It has q and t! " << endl;
                        try{
                                DEBUG_STREAM << "t index " << parent.TimestampIndex << endl;
                                pts = Timestamp_createFromByteArray(
                                        MmsValue_getUtcTimeBuffer(
                                                list_mms[parent.TimestampIndex]));
                                ts = *pts;
                        }catch(...){
                                ERROR_STREAM << "Polling MMS timestamp failed" << endl;
                                parent.hasQuality = false;

                        }
                        try{
                                q = Quality_fromMmsValue(list_mms[parent.QualityIndex]);
                        }catch(...){
                                ERROR_STREAM << "Polling MMS Quality failed" << endl;
                                parent.hasQuality = false;
                        }
                }

                int i=0;
                for (std::vector<iecVariable>::iterator it=mlistOfDAs.begin();
                     it != mlistOfDAs.end(); ++it){
                        iecVariable davar = *it;

                        DEBUG_STREAM << "Checking in MMS:" << davar.da_name << endl;

                        if(davar.da_name.compare(addr))
                        {
                                idp->set_iecValue(list_mms[i]);
                                if(parent.hasQuality){
                                        idp->set_iecQ(q);
                                        idp->set_iecT(ts);
                                }else{
                                        idp->validate();
                                }
                                return true;
                        }
                        i++;
                }

                return true;
        }

}
