
# Table of Contents

1.  [A very short introduction](#org1f991ac)
2.  [Some context](#org7b49677)
3.  [Project description](#orgdb75125)
4.  [Tango controls introduction](#orge8756df)
5.  [IEC4Tango Modeling](#org4bde8d2)
    1.  [IEC 61850 Modeling](#orge98d0d5)
    2.  [Tango modeling](#org1dc736b)
6.  [Device Server architecture](#org9f40217)
7.  [Current project state](#org1fd8966)
8.  [User interface snapshots](#orga8d8384)
9.  [References](#orgfd42291)
10. [Contact](#org10b0628)



<a id="org1f991ac"></a>

# A very short introduction

*IEC4Tango* is a project offering a real open-source alternative to
build industrial supervision systems, distributed control systems and
applications for energy. It is designed to work on embedded
devices (centralized or distributed)  or either
hosted by any kind of clustered virtualization platform.  

The same libraries and design concepts used by *IEC4Tango* can be
also applied for other industrial domains. It's really worth looking into
the software pieces mentioned in this project, mainly [tango controls](https://www.tango-controls.org)
platform. If you aren't interested in energy applications or if you
need a better overview of what's behind *IEC4Tango*, reserve some time
to check also tango controls project in their website.


<a id="org7b49677"></a>

# Some context

In order to face the challenges of the *sustainable power grid of the
future*, distributed intelligence across the whole energy supply chain
is necessary. That is the *Smart Grid*. Challenges exist from production
to consumption: dealing with intermittence, power stabilization,
business optimization, market and regulatory procedures, energy
transport, distribution and others.  It is clear that these
decentralized applications composing the smart grid must communicate
one with each other, and a common language is vital to make it
viable. All domains and zones composing the power grid are described
by the *Smart Grid Architecture Model* presented in Figure
[4](#org31d5ffd). Communication is necessary between all dimensions and
levels.  The interoperability communications layers are also described
in figure.

![img](SGAM.png "Smart Grid Architecture Model from [1].")

There are two international standards that provide a semantic model to
address the communication interoperability between smart grid domains and zones: *IEC
61850* and *CIM*.  

*IEC4Tango* is based mainly *IEC 61850* standard.  Precisely,
*IEC4Tango*'s scope is represented by shadowed volume of the *Smart
Grid Architecture Model* in Figure [4](#org31d5ffd) (the layers concerned are
marked with a star). It targets applications inside substations and
power plants.

As an alternative, if someone is looking for a open-source Smart Grid
platform to control, manage and monitor devices distributed in public
network, there is the [open smart grid platform](https://opensmartgridplatform.org/) project. However, the
utilization of [Tango Rest Interface](https://tango-controls.readthedocs.io/en/latest/development/advanced/rest-api.html) can also be an option to create
access through public internet or a WAN. The choice depends on the use case.


<a id="orgdb75125"></a>

# Project description

*IEC4Tango* is an open source tango device server and tango device
library designed to supervise *IEC 61850* devices (IEDs) and to make
an industrial (or a scientific) open source SCADA system.  *IEC4Tango* is
based on [tango controls](https://www.tango-controls.org) platform and *IEC 61850* data modeling.  It
constitutes the elementary unit to create industrial distributed
control systems, smart grid applications and supervisory systems targeting the
energy domain.

For those who are unaware, the *IEC 61850* is a communication and
automation standard from the International Electrotechnical
Commission’s (IEC) Technical Committee 57 mainly designed for electric
power systems. The IEC 61850 standard cannot be understood merely as
a communication protocol for control and supervision of substations
and power plants. In reality, it comprises not only protocols in its
characterization, but also communication technologies, services,
object-oriented information models and a powerful inter-operable
configuration language based on *xml*. Used wisely, the IEC 61850
has the power to impact the overall process of control system projects and energy
management.

In brief, it is a vital standard for utility industry and grid
operators.  It is undoubtedly one of the main enablers of the
interoperability of Smart Grid applications. The IEC 61850 information model can
be used across energy management systems, predictive maintenance
algorithms, internet of things, data analytic, optimization
algorithms, asset management and, not to forget, SCADA systems.

In spite of IEC 61850 latent potential, nowadays most distributed
control and supervisory systems are not well designed to handle and to
treat all the information available regarding IEC 61850 devices. There are technical
challenges (handling large data sets) as well as commercial challenges
(proprietary SCADA business models not adapted to IEC 61850). 

*IEC4Tango* aims to unleash the IEC 61850 data modeling potential
making possible the development of *on-premises* and *cloud*
applications for the power grid of the future.


<a id="orge8756df"></a>

# Tango controls introduction

[Tango Controls](https://www.tango-controls.org) is a mature open source framework solution that can be
used to build object oriented *SCADA* and *DCS* platforms. It has been
around for a while (more than 17 years) and nowadays it's mainly used in
complex scientific installations such as *synchrotrons* (cyclic
particle accelerator installations). It is a serious project. Tango
controls is still very active today (next collaboration meeting is
supposed to be held in June 2020).

From the online Tango controls documentation, we can highlight the following important features:

-   CORBA and ZMQ protocols to communicate between tango device server and clients (tango backbone)
-   C++, Python and Java as reference programming languages
-   Linux and Windows as operating systems
-   Modern object oriented design patterns
-   Naturally implements a microservices architecture
-   Unit tested, continuous integration enabled
-   Hosted on Github (<https://github.com/tango-controls>)
-   Extensive documentation + tools, large community

In addition, several Tango-related projects provides extra capabilities:

-   Capacity to easily create complex user graphic interfaces (based on Qt5 library or Swing library)
-   An historical archive system (based on *Mysql*, *Cassandra* or *Postgresql* data bases)
-   An alarm management system
-   Tango model based Rest API (enabling web applications and html interfaces)
-   Python, Matlab, LabView bindings (useful to build complex applications such as data analytics, deep learning and optimization algorithms)
-   A log management system (to track, debug and supervise the control system itself)
-   and others

In addition, Tango controls platform can be completely containerized in a
distributed architecture (fully designed as microservices). This kind of
architecture improves the overall availability of the system,
facilitates remote maintenance and integrates well with DevOps
development methodologies. Such a platform can be easily hosted in
virtual cluster infrastructures such as the one provided by
*Kubernetes* + *Docker* + *KVM*.


<a id="org4bde8d2"></a>

# IEC4Tango Modeling

IEC4Tango can be defined in simple terms as an *IEC 61850*
communication driver for tango controls.  Both IEC 61850 and Tango
communication protocols are strongly based on an object-oriented
approach. But since their modeling structures and semantics are not
exactly the same, a conversion between models is necessary.  Some
introduction may be necessary here.


<a id="orge98d0d5"></a>

## IEC 61850 Modeling

The basic building blocks of the *IEC 61850* data modeling are:
*IEDs*, *Logical Devices*, *Logical Nodes*, *Data Objects* and *Data
Attributes*.  The relationship among these elements is succinctly
presented in Figure [34](#orgd5b053d).

![img](ChIntro_IED_simple_shema.png "IEC 61850 data modeling simplified schema.")

-   **IED::** An *Intelligent Electronic Device* represents every equipment that
    can be connected to an *IEC 61850* network incorporating one or more
    processors, able to receive or send data/control
    from, or to, an external source. The IED should be capable of
    executing the behaviour of one or more, specified logical nodes in a
    particular context and delimited by its interfaces.

-   **Logical Device::** A *Logical Device* is mainly a repository of
    *Logical Nodes* and a symbolic container of services (for
    example GOOSE control blocks, sampled value control blocks, setting
    groups, reports, etc). The content of a *Logical Device* can
    eventually represent an entire function as well as having any
    functional representation at all.  The functional content of
    *Logical Devices* and their names are not standardized.

-   **Logical Node::** A *Logical Node* represents the data modeling and
    the services of an atomic function or a physical equipment. It is the
    main object-modeling building block with defined semantics in the
    standard.  In other words, a *Logical Node* represents an
    indivisible part of a function or equipment that exchanges
    standardized data. It is an object defined by its data and
    methods. The instantiation of *Logical Nodes* can be highly
    distributed : spread in several IEDs,
    working together and exchanging data to produce one or more
    high-level functionality.

-   **Data Object::** *Data Objects* are structured and typed data hosted
    by *Logical Nodes*. It represents for instance the information of the position of a
    circuit breaker together with its properties, description, 
    quality, timestamps, etc. Together with the *Logical Node* it
    provides standardized semantics. *Data Objects* provides services
    such as control and commands (used to affect the state of the *Data
    Object*, like *open* or *close*).

-   **Data Attribute::** *Data Objets* are the composition of *Data
    Attributes*. *Data Attribute* can represent a basic type variable
    (boolean, integer, float, etc) or a structure of variables. Together
    with *Logical Nodes* and *Data Objects* they provide standardized
    semantics to the information.


<a id="org1dc736b"></a>

## Tango modeling

Tango modeling is better explained in 
[tango docmentation page.](https://tango-controls.readthedocs.io/en/latest/overview/SimplifiedTangoDatamodel.html) The conversion from IEC 61850 modeling to Tango
modeling is described below:

-   ***IEC4Tango* Device Server::** it is an executable file that hosts
    different tango devices instances. *IEC4Tango* Device Server can
    instantiate three types of tango devices (part of IEC4Tango
    project): a *Logical Node* tango device, a *Data Object* tango
    device and an *IEC 61850 client* tango device. Each *IEC4Tango*
    Device Server must represent one *IEC 61850* IED. If the system has
    10 IEDs, this server shall be instantiated 10 times. This is a
    design option to create a modular distributed system and
    consequently, to increase overall availability.

-   ***IEC4Tango Logical Node Tango Device*::** it represents an *IEC 61850
    Logical Node* instance. The device has a class which is equivalent
    of a *Logical Node Class*. The tango attributes represents *Data
    Objects* (with quality and timestamp integrated). Whenever
    necessary, different tango attributes are used to represent
    different attributes of a complex *Data Object*.

-   ***IEC4Tango Data Object Tango Device*::** Some *Data Objects* are 
    complex, having different *Data Attributs* or hosting others *Data
    Objects*. When convenient, these *Data Objetcs* are also modeled as
    a tango device.

-   ***IEC4Tango IEC 61850 client*::** it is a tango device responsible to
    manage the IEC 61850 communication stack. 
    It is mandatory to instantiate this device for every *IEC4Tango* Device
    Server registered in the system (otherwise the devices will not
    communicate with the IED).

The Figure [46](#org3ef99cb) presents an example of a tango control system using
*IEC4Tango* to supervise a Circuit Breaker *Logical Node*
(*CSWI*), a electrical measure *Logical Node* (*MMXU*) and a complex voltage measurement *Data
Object* (*MMXU.PhV*).

![img](model.png "Tango and IEC 61850 models together.")

The name of tango device instances must respect the following rules: 

`{LogicalDevice Name}/{Logical Node Class}/{Logical Node Name}`

or 

`{LogicalDevice Name}/{Data Object CDC}/{Logical Node + Data Object names}`


<a id="org9f40217"></a>

# Device Server architecture

The *IEC4Tango* device server uses the open source [libIEC61850](http://libiec61850.com/libiec61850/) 
library behind the scenes. It is a stable *IEC 61850* communication
stack  well suited for embedded implementations with low resource
footprint. In addition, libIEC61850 can be compiled together with [mbedtls](https://github.com/ARMmbed/mbedtls)
library, providing TLS encryption to *MMS* communications (*IEC 61850*
communication protocol): cyber-security matters to *IEC4Tango*.

The Figure [60](#org4a86e28) presents the *IEC4tango* device server architecture.

Basically, the *IEC4Tango IEC 61850 client* device is responsible to
manage the libIEC61850 client communication stack. Once
an association with the IED is established, the device enables a reserved *MMS* report
control block and start waiting for new report events (spontaneous events or integrity
reports).

A report handler is used the check the report events and send events
messages to a message queue. This mecanism is important to quickly
release the communication stack.

Another thread is used to check the message queue and update the
*real-time memory based data base*. This thread is also managed by
*IEC4Tango IEC 61850 client* device. Here *IEC 61850* data types are
converted into Tango data types. Source quality and timestamp
are preserved.

The real-time model data-base represents the current process
data. This is the information that must be communicated all Tango
servers and clients.  Two mechanisms to forward the information are
used:

1.  Tango [pooling thread mechanism](https://tango-controls.readthedocs.io/en/latest/development/device-api/device-polling.html): a special thread that will
    cyclically get the data from the real-time data base and feed
    internally all *IEC4Tango* devices (Tango devices representing *Logical
    Nodes* and *Data Objects*).
2.  Spontaneous [`fire_archive_event`](https://tango-controls.github.io/cppTango-docs/classTango_1_1Attribute.html#ab008123b44bdb2a13e2cd2c362617e1e), a method that bypasses the pooling mechanism
    and can be used to communicate events to Tango clients desiring to receive them as soon as
    they are captured by the device server.

When this process data is available to the Tango devices, the sky is the limit.

![img](mainschema.png "*IEC4tango* device server architecture.")


<a id="org1fd8966"></a>

# Current project state

*IEC4Tango* device server is currently partially functional. Some
tests are still necessary to assure that the device server is really
stable to real production environments and some progress is also
needed to make it easily managed and configured.

The following tasks are pending (the next steps):

-   **To create a full tango distribution:** It is important to build an
    complete Tango distribution with all functionalities necessary to
    build a complete industrial SCADA for energy. The idea is to
    organize in one place all tango related projects with the objective
    to have a tango distribution not only with a standalone *IEC4Tango*
    device server, but with all user interfaces, archive system, alarm
    system, log system, and others. The goal is also to make them
    available in easily deployed containers (docker).

-   **To create a library of widgets:** To create graphical elements
    representing logical nodes, logical devices, user interfaces, all
    based on [Taurus SCADA project](https://taurus-scada.org/). Taurus is a very interesting project
    based on [Qt library](https://www.qt.io/) and python. With Taurus, it's possible to create
    complex graphical user interfaces. It is not difficult to create
    animated one-line diagrams representing large substations. They can
    be automatically created from an *IEC 61850* *SCL* language file
    containing the topology description.

-   **To use tango pipes:** pipes can be used to read *Logical Node*
    configuration, asset and description information. A tango pipe actually 
    allows reading and/or writing structured data from and/or to a
    device. The data may be built out of several basic Tango
    datatypes. The structure of data is defined by a device class and is
    not fixed. It may be changed at runtime by the device itself or
    modified upon request from a client. This is useful to
    communicate (when necessary) configuration and descriptive
    information available on *IEC 61850* models.

-   **To create a complete library of tango devices:** It is still
    necessary to model a complete substation system with IEC4Tango.  The
    objective is to be able to model *Rte R#SPACE system*. Check this
    project here:
    ([R#SPACE
    IEC 61850 Model description](https://www.rte-france.com/fr/document/controle-commande-des-postes-rte-modelisation-iec-61850)).

-   **To create an OPC UA gateway:** *OPC UA* is a core communication
    technology behind the majority of modern SCADA systems and *IoT*
    platforms. An *OPC UA* client driver is already available for Tango
    controls, but a server is not available (neither such a server seems
    to be part of tango project scope). It would be a great idea
    to build an *OPC UA* server on the top of *IEC4Tango* library. This
    would increase a lot the interoperability to different SCADAS and
    IoT platforms. The idea is to create an OPC UA server using Tango as
    client and mirroring *IEC4Tango* devices modeling. This is something
    feasible, interesting but still to be planned.


<a id="orga8d8384"></a>

# User interface snapshots

The full capacity of the Taurus SCADA + Qt library is unfortunately
(still) not demonstrated in this section. Some examples are presented
here only to give a small taste of the alternatives to build graphical
user interfaces.

For example, it is very simple to create animated user interfaces
using [JDraw tool](https://tango-controls.readthedocs.io/en/9.2.5/tools-and-extensions/jdraw/jdraw.html).  This tool is a java application used to create
WYSIWYG Tango graphical user interfaces.  An example created in few
minutes to represent a circuit breaker is presented in Figure
[71](#org430f6f3).

Believe me that the buttons with the commands *open* and *close* are
really functional! Commands are already managed by *IEC4Tango* server
device.  In this case, the server device sends *IEC 61850*
control services to the *IED* (*direct control with normal security*
command).

![img](swing.png "Tango java synoptics")

Figure [74](#orgd8108d4) shows a Taurus Panel showing Circuit Breaker
information (this panel was build in 1 minute!).  In this example, the
voltage measurement is accusing an *ALARM* because the *Logical Node*
*Health* is also in alarm state.  These alarms are automatically
managed by the *IEC4Tango* library.  All Tango devices have a state
information with a syntheses of the device current status. At a glance one can
check what is happening with the device.

For example, in Figure [74](#orgd8108d4), the device representing the Circuit
Breaker is showing the state as *CLOSE* (check the upper-left area of the Figure).
The translation between Tango states and *IEC 61850* data model is
automatically managed by the library. In the case of the
*CSWI Logical Node*, the association is presented below:

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Tango state</th>
<th scope="col" class="org-left">IEC 61850</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">On</td>
<td class="org-left">Not used</td>
</tr>


<tr>
<td class="org-left">Off</td>
<td class="org-left">Not used</td>
</tr>


<tr>
<td class="org-left">Close</td>
<td class="org-left">CB is closed</td>
</tr>


<tr>
<td class="org-left">Open</td>
<td class="org-left">CB is opened</td>
</tr>


<tr>
<td class="org-left">Insert</td>
<td class="org-left">Not used</td>
</tr>


<tr>
<td class="org-left">Extract</td>
<td class="org-left">Not used</td>
</tr>


<tr>
<td class="org-left">Moving</td>
<td class="org-left">CB is moving</td>
</tr>


<tr>
<td class="org-left">Standby</td>
<td class="org-left">Not used</td>
</tr>


<tr>
<td class="org-left">Fault</td>
<td class="org-left">Position is in bad-state or Health is alarm</td>
</tr>


<tr>
<td class="org-left">Init</td>
<td class="org-left">Device is starting</td>
</tr>


<tr>
<td class="org-left">Running</td>
<td class="org-left">Not used</td>
</tr>


<tr>
<td class="org-left">Alarm</td>
<td class="org-left">Health is Warning</td>
</tr>


<tr>
<td class="org-left">Disable</td>
<td class="org-left">Behaviour is OFF</td>
</tr>


<tr>
<td class="org-left">Unknown</td>
<td class="org-left">Quality is invalid</td>
</tr>
</tbody>
</table>

![img](taurus.png "Tango taurus synoptics")


<a id="orgfd42291"></a>

# References

[1] Dänekas, Christian & Neureiter, Christian & Rohjans, Sebastian & Uslar, Mathias & Engel, Dominik. (2014). Towards a Model-Driven-Architecture Process for Smart Grid Projects. 261. 47-58. 10.1007/978-3-319-04313-5<sub>5</sub>. 

-   


<a id="org10b0628"></a>

# Contact

Please, if you are interested in the project, feel free to contact me at [acpadoanjr@yahoo.com.br](mailto:acpadoanjr@yahoo.com.br)

