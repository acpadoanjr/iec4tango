#include "iec61850_client.h"
#include "hal_thread.h"

#include <stdlib.h>
#include <stdio.h>


static void commandTerminationHandler(void *parameter, ControlObjectClient connection)
{

        printf("termination \n");
        LastApplError lastApplError = ControlObjectClient_getLastApplError(connection);

        // if lastApplError.error != 0 this indicates a CommandTermination-
        if (lastApplError.error != 0) {
                printf("Received CommandTermination-.\n");
                printf(" LastApplError: %i\n", lastApplError.error);
                printf("      addCause: %i\n", lastApplError.addCause);
        }
        else
                printf("Received CommandTermination+.\n");
}

int main(int argc, char** argv) {

        char* hostname;
        int tcpPort = 102;

        if (argc > 1)
                hostname = argv[1];
        else
                hostname = "localhost";

        if (argc > 2)
                tcpPort = atoi(argv[2]);

        IedClientError error;

        IedConnection con = IedConnection_create();

        IedConnection_connect(con, &error, hostname, tcpPort);



        if (error == IED_ERROR_OK) {


                /************************
                 * Direct control
                 ***********************/

                ControlObjectClient control
                        = ControlObjectClient_create("BCULDDJ/CSWI1.Pos", con);

                ControlObjectClient control2
                        = ControlObjectClient_create("BCULDDJ/ATCC1.TapChg", con);


                ControlObjectClient_setCommandTerminationHandler(control, commandTerminationHandler, NULL);

                MmsValue* ctlVal = MmsValue_newBoolean(true);
                MmsValue* ctlVal1 = MmsValue_newBitString(2);
                MmsValue_setBitStringFromInteger(ctlVal1, 1);


                ControlObjectClient_setOrigin(control, "client", 1);


                if (ControlObjectClient_operate(control, ctlVal, 0 /* operate now */)) {
                        printf("BCULDDJ/CSWI1.Pos operated successfully\n");
                }
                else {
                        printf("failed to operate BCULDDJ/CSWI1.Pos\n");

                }

                if (ControlObjectClient_operate(control2, ctlVal1, 0 /* operate now */)) {
                        printf("BCULDDJ/ATCC1 operated successfully\n");
                }
                else {
                        printf("failed to operate BCULDDJ/ATCC1\n");

                }


                Thread_sleep(1000);

                LastApplError lastApplError = ControlObjectClient_getLastApplError(control);

                // if lastApplError.error != 0 this indicates a CommandTermination-
                if (lastApplError.error != 0) {
                        printf("Received CommandTermination-.\n");
                        printf(" LastApplError: %i\n", lastApplError.error);
                        printf("      addCause: %i\n", lastApplError.addCause);
                }
                else
                {
                        printf("Received CommandTermination+.\n");
                        printf(" LastApplError: %i\n", lastApplError.error);
                        printf("      addCause: %i\n", lastApplError.addCause);
                }

                /* Check if status value has changed */

                MmsValue* stVal = IedConnection_readObject(con, &error, "BCULDDJ/CSWI1.Pos.stVal", IEC61850_FC_ST);

                if (error == IED_ERROR_OK) {
                        int state = MmsValue_getBitStringAsInteger(stVal);

                        printf("New status of simpleIOGenericIO/GGIO1.SPCSO1.stVal: %i\n", state);
                }
                else {
                        printf("Reading status for simpleIOGenericIO/GGIO1.SPCSO1 failed!\n");
                }
                MmsValue_delete(stVal);


                Thread_sleep(1000);

///////////////////////////////////////////////////////////////////////////////////////////////

                ControlObjectClient_setOrigin(control, "client", 2);

                if (ControlObjectClient_operate(control, ctlVal, 1 /* operate now */)) {
                        printf("BCULDDJ/CSWI1.Pos operated successfully\n");
                }
                else {
                        printf("failed to operate BCULDDJ/CSWI1.Pos\n");

                }

                MmsValue_delete(ctlVal);

                Thread_sleep(500);

                lastApplError = ControlObjectClient_getLastApplError(control);

                // if lastApplError.error != 0 this indicates a CommandTermination-
                if (lastApplError.error != 0) {
                        printf("Received CommandTermination-.\n");
                        printf(" LastApplError: %i\n", lastApplError.error);
                        printf("      addCause: %i\n", lastApplError.addCause);
                }
                else
                {
                        printf("Received CommandTermination+.\n");
                        printf(" LastApplError: %i\n", lastApplError.error);
                        printf("      addCause: %i\n", lastApplError.addCause);
                }

                /* Check if status value has changed */

                stVal = IedConnection_readObject(con, &error, "BCULDDJ/CSWI1.Pos.stVal", IEC61850_FC_ST);

                if (error == IED_ERROR_OK) {
                        int state = MmsValue_getBitStringAsInteger(stVal);

                        printf("New status of BCULDDJ/CSWI1.Pos.stVal: %i\n", state);
                }
                else {
                        printf("Reading status BCULDDJ/CSWI1.Pos.stVal failed!\n");
                }

                MmsValue_delete(stVal);


                ControlObjectClient_destroy(control);

                //Testing Others commands

                LinkedList lndos = IedConnection_getLogicalNodeDirectory(con,&error,"BCULDDJ/CSWI1", ACSI_CLASS_DATA_OBJECT);
                LinkedList lnvariables = IedConnection_getLogicalNodeVariables(con,&error,"BCULDDJ/MMXU1");
                // get list of children (only 1 level). Works from DO (not ln):
                LinkedList dodir = IedConnection_getDataDirectoryByFC(con,&error,"BCULDDJ/MMXU1.PhV.phsA",IEC61850_FC_MX);

                if (error == IED_ERROR_OK) {
                        int i;
                        for (i = 0; i < LinkedList_size(lnvariables); i++){
                                LinkedList entry = LinkedList_get(lnvariables, i);
                                //string entryName((char*) entry->data);
                                printf("variable %i : %s \n",i, (char*) entry->data);
                        }

                }
                else {
                        printf("Reading BCULDDJ/CSWI1 failed!\n");
                }


        }
        else {
                printf("Connection failed!\n");
        }

        IedConnection_destroy(con);
}
