/*
 * static_model.c
 *
 * automatically generated from BCU.icd
 */
#include "static_model.h"

static void initializeValues();

extern DataSet iedModelds_LDDJ_LLN0_DS00;


extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda0;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda1;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda2;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda3;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda4;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda5;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda6;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda7;
extern DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda8;

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda0 = {
  "LDDJ",
  false,
  "CSWI1$ST$Pos",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda1
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda1 = {
  "LDDJ",
  false,
  "CSWI1$ST$Loc",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda2
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda2 = {
  "LDDJ",
  false,
  "CSWI1$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda3
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda3 = {
  "LDDJ",
  false,
  "CSWI1$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda4
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda4 = {
  "LDDJ",
  false,
  "MMXU1$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda5
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda5 = {
  "LDDJ",
  false,
  "MMXU1$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda6
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda6 = {
  "LDDJ",
  false,
  "MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda7
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda7 = {
  "LDDJ",
  false,
  "MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  &iedModelds_LDDJ_LLN0_DS00_fcda8
};

DataSetEntry iedModelds_LDDJ_LLN0_DS00_fcda8 = {
  "LDDJ",
  false,
  "ATCC1$ST$TapChg",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDDJ_LLN0_DS00 = {
  "LDDJ",
  "LLN0$DS00",
  9,
  &iedModelds_LDDJ_LLN0_DS00_fcda0,
  NULL
};

LogicalDevice iedModel_LDDJ = {
    LogicalDeviceModelType,
    "LDDJ",
    (ModelNode*) &iedModel,
    NULL,
    (ModelNode*) &iedModel_LDDJ_LLN0
};

LogicalNode iedModel_LDDJ_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_CSWI1,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
};

DataObject iedModel_LDDJ_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_LDDJ_LLN0,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_origin,
    0
};

DataAttribute iedModel_LDDJ_LLN0_Mod_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_ctlNum,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_origin,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Mod_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_LLN0,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_LLN0,
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    (ModelNode*) &iedModel_LDDJ_LLN0_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_LDDJ_LLN0,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_LDDJ_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt_ldNs,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_NamPlt_ldNs = {
    DataAttributeModelType,
    "ldNs",
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt_configRev,
    NULL,
    0,
    IEC61850_FC_EX,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_NamPlt_configRev = {
    DataAttributeModelType,
    "configRev",
    (ModelNode*) &iedModel_LDDJ_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_LLN0_Loc = {
    DataObjectModelType,
    "Loc",
    (ModelNode*) &iedModel_LDDJ_LLN0,
    NULL,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_stVal,
    0
};

DataAttribute iedModel_LDDJ_LLN0_Loc_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LLN0_Loc_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LLN0_Loc,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_CSWI1 = {
    LogicalNodeModelType,
    "CSWI1",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_MMXU1,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
};

DataObject iedModel_LDDJ_CSWI1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_CSWI1,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_CSWI1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_CSWI1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_CSWI1,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_CSWI1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_CSWI1_Loc = {
    DataObjectModelType,
    "Loc",
    (ModelNode*) &iedModel_LDDJ_CSWI1,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_stVal,
    0
};

DataAttribute iedModel_LDDJ_CSWI1_Loc_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Loc_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Loc,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_CSWI1_Pos = {
    DataObjectModelType,
    "Pos",
    (ModelNode*) &iedModel_LDDJ_CSWI1,
    NULL,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    0
};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_ctlNum,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_origin,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper_Check,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Oper,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_origin,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_ctlNum,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_origin,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_Cancel,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_ctlNum,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_origin,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_CODEDENUM,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_ctlModel,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_opRcvd,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_opOk,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_tOpOk,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_subEna,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_CODEDENUM,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_pulseConfig = {
    DataAttributeModelType,
    "pulseConfig",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos,
    NULL,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig_cmdQual,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_pulseConfig_cmdQual = {
    DataAttributeModelType,
    "cmdQual",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig_onDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_pulseConfig_onDur = {
    DataAttributeModelType,
    "onDur",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig_offDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_pulseConfig_offDur = {
    DataAttributeModelType,
    "offDur",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig,
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig_numPls,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_CSWI1_Pos_pulseConfig_numPls = {
    DataAttributeModelType,
    "numPls",
    (ModelNode*) &iedModel_LDDJ_CSWI1_Pos_pulseConfig,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_MMXU1 = {
    LogicalNodeModelType,
    "MMXU1",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_MMXU2,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
};

DataObject iedModel_LDDJ_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_MMXU1,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_MMXU1,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_LDDJ_MMXU1,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_instMag,
    0
};

DataAttribute iedModel_LDDJ_MMXU1_Hz_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_range,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_subMag,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_subMag = {
    DataAttributeModelType,
    "subMag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_subMag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_subMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_subMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_db,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_units,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_sVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_sVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_d,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_Hz_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_Hz,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_LDDJ_MMXU1,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_LDDJ_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_range,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_db,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_units,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsA_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_range,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_db,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_units,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsB_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_d,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_range,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_db,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_units,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_phsC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV_phsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU1_PhV_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU1_PhV,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_MMXU2 = {
    LogicalNodeModelType,
    "MMXU2",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_MMXU3,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
};

DataObject iedModel_LDDJ_MMXU2_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_MMXU2,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU2_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU2_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_MMXU2,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU2_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU2_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_LDDJ_MMXU2,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_instMag,
    0
};

DataAttribute iedModel_LDDJ_MMXU2_Hz_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_range,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_subMag,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_subMag = {
    DataAttributeModelType,
    "subMag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_subMag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_subMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_subMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_db,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_units,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_sVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_sVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_d,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_Hz_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_Hz,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU2_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_LDDJ_MMXU2,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    0
};

DataObject iedModel_LDDJ_MMXU2_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_range,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_db,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_units,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsA_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU2_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_range,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_db,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_units,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsB_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU2_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_d,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_range,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_db,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_units,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_phsC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV_phsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU2_PhV_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU2_PhV,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_MMXU3 = {
    LogicalNodeModelType,
    "MMXU3",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_MMXU4,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
};

DataObject iedModel_LDDJ_MMXU3_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_MMXU3,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU3_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU3_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_MMXU3,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU3_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU3_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_LDDJ_MMXU3,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_instMag,
    0
};

DataAttribute iedModel_LDDJ_MMXU3_Hz_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_range,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_subMag,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_subMag = {
    DataAttributeModelType,
    "subMag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_subMag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_subMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_subMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_db,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_units,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_sVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_sVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_d,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_Hz_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_Hz,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU3_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_LDDJ_MMXU3,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    0
};

DataObject iedModel_LDDJ_MMXU3_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_range,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_db,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_units,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsA_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU3_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_range,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_db,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_units,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsB_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU3_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_d,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_range,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_db,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_units,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_phsC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV_phsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU3_PhV_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU3_PhV,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_MMXU4 = {
    LogicalNodeModelType,
    "MMXU4",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_MMXU5,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
};

DataObject iedModel_LDDJ_MMXU4_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_MMXU4,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU4_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU4_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_MMXU4,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU4_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU4_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_LDDJ_MMXU4,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_instMag,
    0
};

DataAttribute iedModel_LDDJ_MMXU4_Hz_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_range,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_subMag,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_subMag = {
    DataAttributeModelType,
    "subMag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_subMag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_subMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_subMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_db,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_units,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_sVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_sVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_d,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_Hz_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_Hz,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU4_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_LDDJ_MMXU4,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    0
};

DataObject iedModel_LDDJ_MMXU4_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_range,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_db,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_units,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsA_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU4_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_range,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_db,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_units,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsB_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU4_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_d,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_range,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_db,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_units,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_phsC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV_phsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU4_PhV_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU4_PhV,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_MMXU5 = {
    LogicalNodeModelType,
    "MMXU5",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_MMXU6,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
};

DataObject iedModel_LDDJ_MMXU5_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_MMXU5,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU5_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU5_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_MMXU5,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU5_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU5_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_LDDJ_MMXU5,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_instMag,
    0
};

DataAttribute iedModel_LDDJ_MMXU5_Hz_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_range,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_subMag,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_subMag = {
    DataAttributeModelType,
    "subMag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_subMag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_subMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_subMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_db,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_units,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_sVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_sVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_d,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_Hz_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_Hz,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU5_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_LDDJ_MMXU5,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    0
};

DataObject iedModel_LDDJ_MMXU5_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_range,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_db,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_units,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsA_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU5_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_range,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_db,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_units,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsB_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU5_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_d,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_range,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_db,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_units,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_phsC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV_phsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU5_PhV_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU5_PhV,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_MMXU6 = {
    LogicalNodeModelType,
    "MMXU6",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_LPHD1,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
};

DataObject iedModel_LDDJ_MMXU6_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDDJ_MMXU6,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU6_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU6_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDDJ_MMXU6,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_stVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU6_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU6_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_LDDJ_MMXU6,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_instMag,
    0
};

DataAttribute iedModel_LDDJ_MMXU6_Hz_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_range,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_subMag,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_subMag = {
    DataAttributeModelType,
    "subMag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_subMag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_subMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_subMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_db,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_units,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_sVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_sVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_d,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_Hz_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_Hz,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU6_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_LDDJ_MMXU6,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    0
};

DataObject iedModel_LDDJ_MMXU6_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_range,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_db,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_units,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsA_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU6_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_range,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_db,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_units,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsB_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_MMXU6_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_d,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal,
    0
};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_instCVal = {
    DataAttributeModelType,
    "instCVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_instCVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_range,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal_ang,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_cVal_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal_ang_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_cVal_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_range = {
    DataAttributeModelType,
    "range",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAng,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAng = {
    DataAttributeModelType,
    "rangeAng",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_q,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subEna,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subCval = {
    DataAttributeModelType,
    "subCval",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subQ,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval_ang,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subCval_ang = {
    DataAttributeModelType,
    "ang",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval_mag,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval_ang_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subCval_ang_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval_ang,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subCval_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval_mag_f,
    0,
    IEC61850_FC_SV,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subCval_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subCval_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_db,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_units,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_units_multiplier,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_units_multiplier = {
    DataAttributeModelType,
    "multiplier",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_db = {
    DataAttributeModelType,
    "db",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_dbAng,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_dbAng = {
    DataAttributeModelType,
    "dbAng",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_zeroDb,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_zeroDb = {
    DataAttributeModelType,
    "zeroDb",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_magSVC,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_magSVC = {
    DataAttributeModelType,
    "magSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_magSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_magSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_magSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_magSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_magSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_magSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_angSVC = {
    DataAttributeModelType,
    "angSVC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_angSVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_angSVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_angSVC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_angSVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_angSVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_angSVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC = {
    DataAttributeModelType,
    "rangeC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC = {
    DataAttributeModelType,
    "rangeAngC",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_smpRate,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hhLim,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hhLim = {
    DataAttributeModelType,
    "hhLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hhLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hhLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hhLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hLim = {
    DataAttributeModelType,
    "hLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_lLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_hLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_lLim = {
    DataAttributeModelType,
    "lLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_llLim,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_lLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_lLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_lLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_llLim = {
    DataAttributeModelType,
    "llLim",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_min,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_llLim_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_llLim_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_llLim,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_min = {
    DataAttributeModelType,
    "min",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_max,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_min_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_min_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_min,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_max = {
    DataAttributeModelType,
    "max",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC,
    NULL,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_max_f,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_max_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_rangeAngC_max,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_smpRate = {
    DataAttributeModelType,
    "smpRate",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_angRef,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_angRef = {
    DataAttributeModelType,
    "angRef",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC_d,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_phsC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV_phsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_MMXU6_PhV_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_MMXU6_PhV,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_LPHD1 = {
    LogicalNodeModelType,
    "LPHD1",
    (ModelNode*) &iedModel_LDDJ,
    (ModelNode*) &iedModel_LDDJ_ATCC1,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
};

DataObject iedModel_LDDJ_LPHD1_PhyHealth = {
    DataObjectModelType,
    "PhyHealth",
    (ModelNode*) &iedModel_LDDJ_LPHD1,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_stVal,
    0
};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_d,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth_blkEna,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_PhyHealth_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_LPHD1_PhyHealth,
    NULL,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_LDDJ_LPHD1_Proxy = {
    DataObjectModelType,
    "Proxy",
    (ModelNode*) &iedModel_LDDJ_LPHD1,
    NULL,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_stVal,
    0
};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_LPHD1_Proxy_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_LPHD1_Proxy,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDDJ_ATCC1 = {
    LogicalNodeModelType,
    "ATCC1",
    (ModelNode*) &iedModel_LDDJ,
    NULL,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
};

DataObject iedModel_LDDJ_ATCC1_TapChg = {
    DataObjectModelType,
    "TapChg",
    (ModelNode*) &iedModel_LDDJ_ATCC1,
    NULL,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    0
};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CODEDENUM,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_ctlNum,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_origin,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper_Check,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Oper,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_ctlModel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CODEDENUM,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_ctlNum,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_origin,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_Cancel,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_ctlNum,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_maxVal,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_maxVal = {
    DataAttributeModelType,
    "maxVal",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_minVal,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT8,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_minVal = {
    DataAttributeModelType,
    "minVal",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT8,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_persistent,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_origin,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_persistent = {
    DataAttributeModelType,
    "persistent",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_q,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_valWTr,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_valWTr = {
    DataAttributeModelType,
    "valWTr",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg,
    NULL,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_valWTr_transInd,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_valWTr_transInd = {
    DataAttributeModelType,
    "transInd",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_valWTr,
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_valWTr_posVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDDJ_ATCC1_TapChg_valWTr_posVal = {
    DataAttributeModelType,
    "posVal",
    (ModelNode*) &iedModel_LDDJ_ATCC1_TapChg_valWTr,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

extern ReportControlBlock iedModel_LDDJ_LLN0_report0;
extern ReportControlBlock iedModel_LDDJ_LLN0_report1;
extern ReportControlBlock iedModel_LDDJ_LLN0_report2;

ReportControlBlock iedModel_LDDJ_LLN0_report0 = {&iedModel_LDDJ_LLN0, "urcb01", "BCULDDJ/LLN0$RP$urcb", false, "DS00", 1, 27, 175, 50, 60000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDDJ_LLN0_report1};
ReportControlBlock iedModel_LDDJ_LLN0_report1 = {&iedModel_LDDJ_LLN0, "urcb02", "BCULDDJ/LLN0$RP$urcb", false, "DS00", 1, 27, 175, 50, 60000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDDJ_LLN0_report2};
ReportControlBlock iedModel_LDDJ_LLN0_report2 = {&iedModel_LDDJ_LLN0, "urcb03", "BCULDDJ/LLN0$RP$urcb", false, "DS00", 1, 27, 175, 50, 60000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, NULL};







IedModel iedModel = {
    "BCU",
    &iedModel_LDDJ,
    &iedModelds_LDDJ_LLN0_DS00,
    &iedModel_LDDJ_LLN0_report0,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    initializeValues
};

static void
initializeValues()
{

iedModel_LDDJ_CSWI1_Pos_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_LDDJ_ATCC1_TapChg_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(1);
}
