/*
 *  server_example_control.c
 *
 *  How to use the different control handlers (TBD)
 */

#include "iec61850_server.h"
#include "hal_thread.h"
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include "static_model.h"
#include "mms_value.h"


/* TODO: timestamp quality */

/* import IEC 61850 device model created from SCL-File */
extern IedModel iedModel;

static int running = 0;
static IedServer iedServer = NULL;

void
sigint_handler(int signalId)
{
        running = 0;
}

static CheckHandlerResult
checkHandler(void* parameter, MmsValue* ctlVal, bool test, bool interlockCheck, ClientConnection connection)
{
        printf("check handler called!\n");

        if (interlockCheck){
                printf("  with interlock check bit set!\n");
                return CONTROL_OBJECT_ACCESS_DENIED_INTERLOCK;
        }


        MmsValue* org = NULL;
        MmsValue* dob = NULL;
        MmsValue* orCat = NULL;

        dob = IedServer_getFunctionalConstrainedData((IedServer)parameter,IEDMODEL_LDDJ_CSWI1_Pos, FunctionalConstraint_fromString("ST"));
        int max = MmsValue_getArraySize(dob);
        int i;
        /* for (i = 0; i < max; ++i) { */
        /*         org = MmsValue_getElement(dob,i); */
        /*         printf("type %i : %s \n",i,MmsValue_getTypeString(org)); */

        org = MmsValue_getElement(dob,0);
        orCat = MmsValue_getElement(org,0);
        int iorCat = (int) MmsValue_toInt32(orCat);

        max = MmsValue_getArraySize(org);
        /* for (i = 0; i < max; ++i) { */
        /*         dob = MmsValue_getElement(org,i); */
        /*         printf("type %i : %s \n",i,MmsValue_getTypeString(dob)); */
        /* } */
        printf("orCat : %i \n", iorCat);

        if (orCat == NULL)
        {
                return CONTROL_OBJECT_UNDEFINED;
        }
        else
        {
                if (iorCat == 2)
                        return CONTROL_ACCEPTED;
                else
                        return CONTROL_OBJECT_ACCESS_DENIED_ORCAT;
        }

        return CONTROL_OBJECT_UNDEFINED;
}

static ControlHandlerResult
controlHandlerForBinaryOutput(void* parameter, MmsValue* value, bool test)
{
        Quality qu;
        qu = QUALITY_VALIDITY_GOOD;
        Dbpos pos;
        uint64_t timestampi;
        Timestamp iecTimestampi;



        if (parameter == IEDMODEL_LDDJ_CSWI1_Pos) {
                if (MmsValue_getBoolean(value))
                {
                        pos=DBPOS_ON;
                }
                else
                {
                        pos=DBPOS_OFF;
                }
                Dbpos pos_read = Dbpos_fromMmsValue(IedServer_getAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Pos_stVal)) ;
                 printf("pos: %i : %i \n", (int) pos_read, (int) pos);

                if (((int) pos) != pos_read){
                        timestampi = Hal_getTimeInMs();
                        Timestamp_clearFlags(&iecTimestampi);
                        Timestamp_setTimeInMilliseconds(&iecTimestampi, timestampi);
                        qu = QUALITY_VALIDITY_GOOD;
                        IedServer_lockDataModel(iedServer);
                        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Pos_t, &iecTimestampi);
                        IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_CSWI1_Pos_q, qu);
                        IedServer_udpateDbposValue(iedServer, IEDMODEL_LDDJ_CSWI1_Pos_stVal, pos);
                        IedServer_unlockDataModel(iedServer);
                }

        }
        else
                return CONTROL_RESULT_FAILED;
        return CONTROL_RESULT_OK;
}


static ControlHandlerResult
controlHandlerForINT8Output(void* parameter, MmsValue* value, bool test)
{
        Quality qu;
        qu = QUALITY_VALIDITY_GOOD;
        int8_t pos;
        uint64_t timestampi;
        Timestamp iecTimestampi;

        uint32_t ctlVal;

        if (parameter == IEDMODEL_LDDJ_ATCC1_TapChg) {
                printf("BSC oper called \n");
                pos= (int8_t) MmsValue_toInt32(IedServer_getAttributeValue(iedServer, IEDMODEL_LDDJ_ATCC1_TapChg_valWTr_posVal));
                printf("pos read %i \n", pos);
                //ctlVal = MmsValue_getBitStringAsIntegerBigEndian(IEDMODEL_LDDJ_ATCC1_TapChg_Oper_ctlVal->mmsValue);
                ctlVal = (uint32_t) MmsValue_getBitStringAsIntegerBigEndian(value);
                //ctlVal = Dbpos_fromMmsValue(value);
                printf("ctlVal read %u \n", ctlVal);


                if (ctlVal==1 && pos > 0)
                {
                        pos=pos-1;
                }
                if(ctlVal==2 && pos < 16)
                {
                        pos=pos+1;
                }

                printf("pos updated %i \n", pos);

                timestampi = Hal_getTimeInMs();
                Timestamp_clearFlags(&iecTimestampi);
                Timestamp_setTimeInMilliseconds(&iecTimestampi, timestampi);
                qu = QUALITY_VALIDITY_GOOD;
                IedServer_lockDataModel(iedServer);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_ATCC1_TapChg_t, &iecTimestampi);
                IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_ATCC1_TapChg_q, qu);
                MmsValue_setInt8(IEDMODEL_LDDJ_ATCC1_TapChg_valWTr_posVal->mmsValue , pos);
                IedServer_unlockDataModel(iedServer);

        }
        else
                return CONTROL_RESULT_FAILED;
        return CONTROL_RESULT_OK;
}


int
main(int argc, char** argv)
{

        iedServer = IedServer_create(&iedModel);

        IedServer_setControlHandler(iedServer, IEDMODEL_LDDJ_CSWI1_Pos,
                                    (ControlHandler) controlHandlerForBinaryOutput,
                                    IEDMODEL_LDDJ_CSWI1_Pos);

        IedServer_setControlHandler(iedServer, IEDMODEL_LDDJ_ATCC1_TapChg,
                                    (ControlHandler) controlHandlerForINT8Output,
                                    IEDMODEL_LDDJ_ATCC1_TapChg);


        /* this is optional - performs operative checks */
        IedServer_setPerformCheckHandler(iedServer, IEDMODEL_LDDJ_CSWI1_Pos, (ControlPerformCheckHandler) checkHandler,iedServer);


        /* MMS server will be instructed to start listening to client connections. */
        IedServer_start(iedServer, 102);

        if (!IedServer_isRunning(iedServer)) {
                printf("Starting server failed! Exit.\n");
                IedServer_destroy(iedServer);
                exit(-1);
        }

        running = 1;

        signal(SIGINT, sigint_handler);
        Quality q;
        uint64_t timestamp = Hal_getTimeInMs();
        Timestamp iecTimestamp;
        Dbpos position;
        position = DBPOS_OFF;



        timestamp = Hal_getTimeInMs();
        Timestamp_clearFlags(&iecTimestamp);
        Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
        q = QUALITY_VALIDITY_GOOD;
        IedServer_lockDataModel(iedServer);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Pos_t, &iecTimestamp);
        IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_CSWI1_Pos_q, q);
        IedServer_udpateDbposValue(iedServer, IEDMODEL_LDDJ_CSWI1_Pos_stVal, position);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Beh_t, &iecTimestamp);
        IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_CSWI1_Beh_q, q);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Beh_stVal, 1);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_mag_f, 50.01);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_instMag_f, 50.01);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_rangeC_hhLim_f, 50.1);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_rangeC_hLim_f, 50.05);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_rangeC_llLim_f, 49.9);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_rangeC_lLim_f, 49.95);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_rangeC_max_f, 50.2);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_rangeC_min_f, 49.8);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_units_SIUnit, 33);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_units_multiplier, 0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Beh_stVal, 1);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Beh_t, &iecTimestamp);
        IedServer_updateQuality(iedServer, IEDMODEL_LDDJ_MMXU1_Beh_q, q);
        IedServer_updateQuality(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_q, q);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_t, &iecTimestamp);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Health_stVal, 1);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Health_t, &iecTimestamp);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Health_stVal, 1);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Health_t, &iecTimestamp);
        MmsValue_setInt8(IEDMODEL_LDDJ_ATCC1_TapChg_maxVal->mmsValue, 16);
        MmsValue_setInt8(IEDMODEL_LDDJ_ATCC1_TapChg_minVal->mmsValue, 0);
        MmsValue_setInt8(IEDMODEL_LDDJ_ATCC1_TapChg_valWTr_posVal->mmsValue , 8);
        MmsValue_setBoolean(IEDMODEL_LDDJ_ATCC1_TapChg_valWTr_transInd->mmsValue, false);
        IedServer_updateQuality(iedServer, IEDMODEL_LDDJ_ATCC1_TapChg_q , q);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_ATCC1_TapChg_t, &iecTimestamp);





//------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU1_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU2_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU2_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU3_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU3_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU4_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU4_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU5_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU5_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------


        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_cVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_cVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_cVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_cVal_ang_f, 240.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_instCVal_mag_f, 225.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_instCVal_ang_f, 0.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_instCVal_ang_f, 120.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_instCVal_ang_f, 240.0);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_t, &iecTimestamp);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_t, &iecTimestamp);


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_angRef, 12);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_smpRate, 1000);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_db, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_dbAng, 10);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_zeroDb, 10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsA_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsA_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsA_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsA_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsA_d, "Phase A");


        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsB_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsB_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsB_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsB_d, "Phase B");

        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_range, 0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeC_hhLim_f, 227.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeC_hLim_f, 226.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeC_llLim_f, 223.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeC_lLim_f, 224.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeC_max_f, 230.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeC_min_f, 210.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_units_SIUnit, 29);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_units_multiplier, 3);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeAngC_hhLim_f, 2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeAngC_hLim_f, 1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeAngC_llLim_f, -1.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeAngC_lLim_f, -2.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeAngC_max_f, 5.0);
        IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_rangeAngC_min_f, -5.0);
        IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_angRef, 12);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsB_smpRate->mmsValue,1000);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsC_db->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsC_dbAng->mmsValue,10);
        MmsValue_setUint32(IEDMODEL_LDDJ_MMXU6_PhV_phsC_zeroDb->mmsValue,10);
        IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU6_PhV_phsC_d, "Phase C");


//------------------------------------------------------------------------------------------------------------------



        IedServer_unlockDataModel(iedServer);


        while (running) {

                timestamp = Hal_getTimeInMs();
                Timestamp_clearFlags(&iecTimestamp);
                Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
                q = QUALITY_VALIDITY_QUESTIONABLE + QUALITY_DETAIL_OLD_DATA;
                IedServer_lockDataModel(iedServer);
                IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_CSWI1_Loc_q, q);
                IedServer_updateBooleanAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Loc_stVal, 1);
                IedServer_updateTimestampAttributeValue(iedServer,IEDMODEL_LDDJ_CSWI1_Loc_t, &iecTimestamp);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_mag_f, 50.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_instMag_f, 50.02);
                IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_range, 0);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_t, &iecTimestamp);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_cVal_mag_f, 225.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_cVal_mag_f, 225.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_cVal_mag_f, 225.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_cVal_ang_f, 0.0);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_cVal_ang_f, 120.0);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_cVal_ang_f, 240.0);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_instCVal_mag_f, 225.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_instCVal_mag_f, 225.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_instCVal_mag_f, 225.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_instCVal_ang_f, 0.0);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_instCVal_ang_f, 120.0);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_instCVal_ang_f, 240.0);

                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_t, &iecTimestamp);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_t, &iecTimestamp);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_t, &iecTimestamp);

                IedServer_unlockDataModel(iedServer);
                Thread_sleep(10000);


                timestamp = Hal_getTimeInMs();
                Timestamp_clearFlags(&iecTimestamp);
                Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
                Timestamp_setClockNotSynchronized(&iecTimestamp, true);

                q = QUALITY_VALIDITY_INVALID + QUALITY_DETAIL_OSCILLATORY;
                IedServer_lockDataModel(iedServer);
                IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_CSWI1_Loc_q, q);
                IedServer_updateTimestampAttributeValue(iedServer,IEDMODEL_LDDJ_CSWI1_Loc_t, &iecTimestamp);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_mag_f, 50.07);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_instMag_f, 50.07);
                IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_range, 1);
                IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Health_stVal, 2);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Health_t, &iecTimestamp);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_t, &iecTimestamp);
                IedServer_unlockDataModel(iedServer);
                Thread_sleep(10000);

                timestamp = Hal_getTimeInMs();
                Timestamp_clearFlags(&iecTimestamp);
                Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
                q = QUALITY_VALIDITY_GOOD + QUALITY_SOURCE_SUBSTITUTED;
                IedServer_lockDataModel(iedServer);
                IedServer_updateQuality(iedServer,IEDMODEL_LDDJ_CSWI1_Loc_q, q);
                IedServer_updateBooleanAttributeValue(iedServer, IEDMODEL_LDDJ_CSWI1_Loc_stVal, 0);
                IedServer_updateTimestampAttributeValue(iedServer,IEDMODEL_LDDJ_CSWI1_Loc_t, &iecTimestamp);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_mag_f, 49.97);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_instMag_f, 49.97);
                IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_range, 0);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Hz_t, &iecTimestamp);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_cVal_mag_f, 225.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_cVal_mag_f, 225.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_cVal_mag_f, 225.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_cVal_ang_f, 0.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_cVal_ang_f, 120.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_cVal_ang_f, 240.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_instCVal_mag_f, 225.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_instCVal_mag_f, 225.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_instCVal_mag_f, 225.02);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_instCVal_ang_f, 0.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_instCVal_ang_f, 120.3);
                IedServer_updateFloatAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_instCVal_ang_f, 240.3);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsA_t, &iecTimestamp);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsB_t, &iecTimestamp);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_PhV_phsC_t, &iecTimestamp);
                IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Health_stVal, 1);
                IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDDJ_MMXU1_Health_t, &iecTimestamp);


                IedServer_unlockDataModel(iedServer);
                Thread_sleep(10000);



        }

        /* stop MMS server - close TCP server socket and all client sockets */
        IedServer_stop(iedServer);

        /* Cleanup - free all resources */
        IedServer_destroy(iedServer);
} /* main() */
