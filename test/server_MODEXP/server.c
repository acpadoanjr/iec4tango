/*
 *  server_example_control.c
 *
 *  How to use the different control handlers (TBD)
 */

#include <iec61850_server.h>
#include <hal_thread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include "static_model.h"
#include <mms_value.h>


/* TODO: timestamp quality */

/* import IEC 61850 device model created from SCL-File */
extern IedModel iedModel;

static int running = 0;
static IedServer iedServer = NULL;

void
sigint_handler(int signalId)
{
        running = 0;
}






int
main(int argc, char** argv)
{

        iedServer = IedServer_create(&iedModel);


        /* MMS server will be instructed to start listening to client connections. */
        IedServer_start(iedServer, 103);

        if (!IedServer_isRunning(iedServer)) {
                printf("Starting server failed! Exit.\n");
                IedServer_destroy(iedServer);
                exit(-1);
        }

        running = 1;

        signal(SIGINT, sigint_handler);
        Quality q;
        uint64_t timestamp = Hal_getTimeInMs();
        Timestamp iecTimestamp;
        Dbpos position;
        position = DBPOS_OFF;



        timestamp = Hal_getTimeInMs();
        Timestamp_clearFlags(&iecTimestamp);
        Timestamp_setTimeInMilliseconds(&iecTimestamp, timestamp);
        q = QUALITY_VALIDITY_GOOD;

        IedServer_lockDataModel(iedServer);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDMODEXPS_LLN0_LocSta_t , &iecTimestamp);
        IedServer_updateQuality(iedServer,IEDMODEL_LDMODEXPS_LLN0_LocSta_q, q);
        IedServer_updateBooleanAttributeValue(iedServer,IEDMODEL_LDMODEXPS_LLN0_LocSta_stVal,true);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDMODEXPS_GAPC0_SPCSO1_t , &iecTimestamp);
        IedServer_updateQuality(iedServer,IEDMODEL_LDMODEXPS_GAPC0_SPCSO1_q, q);
        IedServer_updateBooleanAttributeValue(iedServer, IEDMODEL_LDMODEXPS_GAPC0_SPCSO1_stVal,false);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDMODEXPS_GAPC0_SPCSO2_t , &iecTimestamp);
        IedServer_updateQuality(iedServer,IEDMODEL_LDMODEXPS_GAPC0_SPCSO2_q, q);
        IedServer_updateBooleanAttributeValue(iedServer, IEDMODEL_LDMODEXPS_GAPC0_SPCSO2_stVal,false);
        IedServer_updateTimestampAttributeValue(iedServer, IEDMODEL_LDMODEXPF_IHMI0_LocSta_t , &iecTimestamp);
        IedServer_updateQuality(iedServer, IEDMODEL_LDMODEXPF_IHMI0_LocSta_q, q);
        IedServer_updateBooleanAttributeValue(iedServer, IEDMODEL_LDMODEXPF_IHMI0_LocSta_stVal,true);
        IedServer_unlockDataModel(iedServer);


        while (running) {

                Thread_sleep(10000);

        }

        /* stop MMS server - close TCP server socket and all client sockets */
        IedServer_stop(iedServer);

        /* Cleanup - free all resources */
        IedServer_destroy(iedServer);
} /* main() */
