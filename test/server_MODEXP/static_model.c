/*
 * static_model.c
 *
 * automatically generated from Modexp.icd
 */
#include "static_model.h"

static void initializeValues();

extern DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT;
extern DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT;
extern DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_EXT;
extern DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT;
extern DataSet iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT;
extern DataSet iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT;
extern DataSet iedModelds_LDMODEXPF_LLN0_RTE_LLN0_GSE_EXT;


extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda0;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda1;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda2;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda3;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda4;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda5;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda6;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda7;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda8;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda9;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda10;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda11;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda12;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda13;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda14;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda15;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda16;

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda0 = {
  "LDMODEXPS",
  false,
  "LLN0$SV$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda1
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda1 = {
  "LDMODEXPS",
  false,
  "LLN0$ST$NamPlt",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda2
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda2 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$Ind1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda3
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda3 = {
  "LDMODEXPS",
  false,
  "GAPC0$SV$Ind1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda4
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda4 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$SPCSO1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda5
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda5 = {
  "LDMODEXPS",
  false,
  "GAPC0$SV$SPCSO1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda6
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda6 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$SPCSO2",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda7
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda7 = {
  "LDMODEXPS",
  false,
  "GAPC0$SV$SPCSO2",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda8
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda8 = {
  "LDMODEXPS",
  false,
  "LLN0$ST$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda9
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda9 = {
  "LDMODEXPS",
  false,
  "LLN0$SV$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda10
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda10 = {
  "LDMODEXPS",
  false,
  "LLN0$SV$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda11
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda11 = {
  "LDMODEXPS",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda12
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda12 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda13
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda13 = {
  "LDMODEXPS",
  false,
  "GAPC0$SV$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda14
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda14 = {
  "LDMODEXPS",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda15
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda15 = {
  "LDMODEXPS",
  false,
  "LLN0$SV$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda16
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda16 = {
  "LDMODEXPS",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT = {
  "LDMODEXPS",
  "LLN0$RTE_LLN0_DS_RPT_DQCHG_EXT",
  17,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda0,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT
};

extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda0;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda1;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda2;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda3;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda4;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda5;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda6;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda7;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda8;

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda0 = {
  "LDMODEXPS",
  false,
  "LLN0$DC$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda1
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda1 = {
  "LDMODEXPS",
  false,
  "GAPC0$DC$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda2
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda2 = {
  "LDMODEXPS",
  false,
  "LLN0$DC$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda3
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda3 = {
  "LDMODEXPS",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda4
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda4 = {
  "LDMODEXPS",
  false,
  "GAPC0$DC$Ind1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda5
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda5 = {
  "LDMODEXPS",
  false,
  "GAPC0$DC$SPCSO1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda6
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda6 = {
  "LDMODEXPS",
  false,
  "GAPC0$DC$SPCSO2",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda7
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda7 = {
  "LDMODEXPS",
  false,
  "LLN0$DC$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda8
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda8 = {
  "LDMODEXPS",
  false,
  "LLN0$DC$Mod",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT = {
  "LDMODEXPS",
  "LLN0$RTE_LLN0_DS_RPT_CYC_EXT",
  9,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda0,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_EXT
};

extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_EXT_fcda0;

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_EXT_fcda0 = {
  "LDMODEXPS",
  false,
  "LLN0$ST$LocSta",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_EXT = {
  "LDMODEXPS",
  "LLN0$RTE_LLN0_GSE_EXT",
  1,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_EXT_fcda0,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT
};

extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda0;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda1;
extern DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda2;

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda0 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$SPCSO2",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda1
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda1 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$Ind1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda2
};

DataSetEntry iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda2 = {
  "LDMODEXPS",
  false,
  "GAPC0$ST$SPCSO1",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT = {
  "LDMODEXPS",
  "LLN0$RTE_LLN0_GSE_INT",
  3,
  &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_GSE_INT_fcda0,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT
};

extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda0;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda1;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda2;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda3;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda4;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda5;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda6;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda7;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda8;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda9;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda10;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda11;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda12;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda13;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda14;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda15;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda16;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda17;

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda0 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda1
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda1 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef2",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda2
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda2 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef3",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda3
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda3 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef4",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda4
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda4 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef5",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda5
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda5 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef6",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda6
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda6 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef7",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda7
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda7 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef8",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda8
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda8 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef9",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda9
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda9 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$InRef10",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda10
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda10 = {
  "LDMODEXPF",
  false,
  "IHMI0$DC$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda11
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda11 = {
  "LDMODEXPF",
  false,
  "IHMI0$DC$Loc",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda12
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda12 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda13
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda13 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda14
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda14 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda15
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda15 = {
  "LDMODEXPF",
  false,
  "IHMI0$DC$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda16
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda16 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda17
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda17 = {
  "LDMODEXPF",
  false,
  "LLN0$DC$LocSta",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT = {
  "LDMODEXPF",
  "LLN0$RTE_LLN0_DS_RPT_CYC_EXT",
  18,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_CYC_EXT_fcda0,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT
};

extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda0;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda1;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda2;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda3;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda4;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda5;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda6;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda7;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda8;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda9;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda10;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda11;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda12;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda13;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda14;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda15;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda16;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda17;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda18;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda19;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda20;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda21;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda22;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda23;
extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda24;

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda0 = {
  "LDMODEXPF",
  false,
  "LLN0$SV$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda1
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda1 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef1",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda2
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda2 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef2",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda3
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda3 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef3",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda4
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda4 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef4",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda5
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda5 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef5",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda6
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda6 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef6",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda7
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda7 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef7",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda8
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda8 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef8",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda9
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda9 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef9",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda10
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda10 = {
  "LDMODEXPF",
  false,
  "LLN0$SP$InRef10",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda11
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda11 = {
  "LDMODEXPF",
  false,
  "IHMI0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda12
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda12 = {
  "LDMODEXPF",
  false,
  "IHMI0$SV$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda13
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda13 = {
  "LDMODEXPF",
  false,
  "IHMI0$ST$Loc",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda14
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda14 = {
  "LDMODEXPF",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda15
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda15 = {
  "LDMODEXPF",
  false,
  "LLN0$SV$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda16
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda16 = {
  "LDMODEXPF",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda17
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda17 = {
  "LDMODEXPF",
  false,
  "LLN0$SV$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda18
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda18 = {
  "LDMODEXPF",
  false,
  "LLN0$ST$NamPlt",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda19
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda19 = {
  "LDMODEXPF",
  false,
  "IHMI0$ST$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda20
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda20 = {
  "LDMODEXPF",
  false,
  "IHMI0$SV$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda21
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda21 = {
  "LDMODEXPF",
  false,
  "LLN0$ST$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda22
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda22 = {
  "LDMODEXPF",
  false,
  "LLN0$SV$LocSta",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda23
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda23 = {
  "LDMODEXPF",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda24
};

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda24 = {
  "LDMODEXPF",
  false,
  "IHMI0$SV$Loc",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT = {
  "LDMODEXPF",
  "LLN0$RTE_LLN0_DS_RPT_DQCHG_EXT",
  25,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT_fcda0,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_GSE_EXT
};

extern DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_GSE_EXT_fcda0;

DataSetEntry iedModelds_LDMODEXPF_LLN0_RTE_LLN0_GSE_EXT_fcda0 = {
  "LDMODEXPF",
  false,
  "LLN0$ST$LocSta",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_LDMODEXPF_LLN0_RTE_LLN0_GSE_EXT = {
  "LDMODEXPF",
  "LLN0$RTE_LLN0_GSE_EXT",
  1,
  &iedModelds_LDMODEXPF_LLN0_RTE_LLN0_GSE_EXT_fcda0,
  NULL
};

LogicalDevice iedModel_LDMODEXPS = {
    LogicalDeviceModelType,
    "LDMODEXPS",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_LDMODEXPF,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0
};

LogicalNode iedModel_LDMODEXPS_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_LDMODEXPS,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
};

DataObject iedModel_LDMODEXPS_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_blkEna,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_blkEna,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_LLN0_InRef1 = {
    DataObjectModelType,
    "InRef1",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_d,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef1_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef1,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_LLN0_InRef2 = {
    DataObjectModelType,
    "InRef2",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_d,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_InRef2_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_InRef2,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_LLN0_LocSta = {
    DataObjectModelType,
    "LocSta",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_T,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_blkEna,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_Check,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_ctlNum,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_opOk,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_opRcvd,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_operTimeout,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_operTimeout = {
    DataAttributeModelType,
    "operTimeout",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_origin,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig = {
    DataAttributeModelType,
    "pulseConfig",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_q,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_cmdQual,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_cmdQual = {
    DataAttributeModelType,
    "cmdQual",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_numPls,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_numPls = {
    DataAttributeModelType,
    "numPls",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_offDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_offDur = {
    DataAttributeModelType,
    "offDur",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_onDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig_onDur = {
    DataAttributeModelType,
    "onDur",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_pulseConfig,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_LocSta_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_LocSta,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_Check,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_origin,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_blkEna,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_T,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_ctlNum,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_opOk,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_opRcvd,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_operTimeout,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_operTimeout = {
    DataAttributeModelType,
    "operTimeout",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_q,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_origin,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_Mod_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_ldNs,
    0
};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_ldNs = {
    DataAttributeModelType,
    "ldNs",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_configRev,
    NULL,
    0,
    IEC61850_FC_EX,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_configRev = {
    DataAttributeModelType,
    "configRev",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_paramRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_paramRev = {
    DataAttributeModelType,
    "paramRev",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_valRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_valRev = {
    DataAttributeModelType,
    "valRev",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt_vendor,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_LDMODEXPS_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDMODEXPS_GAPC0 = {
    LogicalNodeModelType,
    "GAPC0",
    (ModelNode*) &iedModel_LDMODEXPS,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
};

DataObject iedModel_LDMODEXPS_GAPC0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_blkEna,
    0
};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_GAPC0_Ind1 = {
    DataObjectModelType,
    "Ind1",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_subVal,
    0
};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_Ind1_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_Ind1,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_GAPC0_SPCSO1 = {
    DataObjectModelType,
    "SPCSO1",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_T,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_blkEna,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_Check,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_ctlNum,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_opOk,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_opRcvd,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_operTimeout,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_operTimeout = {
    DataAttributeModelType,
    "operTimeout",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_origin,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig = {
    DataAttributeModelType,
    "pulseConfig",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_q,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_cmdQual,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_cmdQual = {
    DataAttributeModelType,
    "cmdQual",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_numPls,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_numPls = {
    DataAttributeModelType,
    "numPls",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_offDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_offDur = {
    DataAttributeModelType,
    "offDur",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_onDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig_onDur = {
    DataAttributeModelType,
    "onDur",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_pulseConfig,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO1_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO1,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPS_GAPC0_SPCSO2 = {
    DataObjectModelType,
    "SPCSO2",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_T,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_blkEna,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_Check,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_ctlNum,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_opOk,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_opRcvd,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_operTimeout,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_operTimeout = {
    DataAttributeModelType,
    "operTimeout",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_origin,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig = {
    DataAttributeModelType,
    "pulseConfig",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_q,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_cmdQual,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_cmdQual = {
    DataAttributeModelType,
    "cmdQual",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_numPls,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_numPls = {
    DataAttributeModelType,
    "numPls",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_offDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_offDur = {
    DataAttributeModelType,
    "offDur",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_onDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig_onDur = {
    DataAttributeModelType,
    "onDur",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_pulseConfig,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPS_GAPC0_SPCSO2_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPS_GAPC0_SPCSO2,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};


LogicalDevice iedModel_LDMODEXPF = {
    LogicalDeviceModelType,
    "LDMODEXPF",
    (ModelNode*) &iedModel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0
};

LogicalNode iedModel_LDMODEXPF_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_LDMODEXPF,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
};

DataObject iedModel_LDMODEXPF_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_blkEna,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_blkEna,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef1 = {
    DataObjectModelType,
    "InRef1",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef1_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef1,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef10 = {
    DataObjectModelType,
    "InRef10",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef10_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef10,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef2 = {
    DataObjectModelType,
    "InRef2",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef2_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef2,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef3 = {
    DataObjectModelType,
    "InRef3",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef3_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef3,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef4 = {
    DataObjectModelType,
    "InRef4",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef4_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef4,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef5 = {
    DataObjectModelType,
    "InRef5",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef5_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef5,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef6 = {
    DataObjectModelType,
    "InRef6",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef6_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef6,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef7 = {
    DataObjectModelType,
    "InRef7",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef7_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef7,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef8 = {
    DataObjectModelType,
    "InRef8",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef8_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef8,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_InRef9 = {
    DataObjectModelType,
    "InRef9",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_d,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_intAddr,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_intAddr = {
    DataAttributeModelType,
    "intAddr",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_purpose,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_255,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_purpose = {
    DataAttributeModelType,
    "purpose",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_setSrcCB,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_setSrcCB = {
    DataAttributeModelType,
    "setSrcCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_setSrcRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_setTstCB,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_setTstCB = {
    DataAttributeModelType,
    "setTstCB",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_setTstRef,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_setTstRef = {
    DataAttributeModelType,
    "setTstRef",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9_tstEna,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_InRef9_tstEna = {
    DataAttributeModelType,
    "tstEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_InRef9,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_LocSta = {
    DataObjectModelType,
    "LocSta",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_T,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_blkEna,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_Check,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_ctlNum,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_opOk,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_opRcvd,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_operTimeout,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_operTimeout = {
    DataAttributeModelType,
    "operTimeout",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_origin,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig = {
    DataAttributeModelType,
    "pulseConfig",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_q,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_cmdQual,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_cmdQual = {
    DataAttributeModelType,
    "cmdQual",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_numPls,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_numPls = {
    DataAttributeModelType,
    "numPls",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_offDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_offDur = {
    DataAttributeModelType,
    "offDur",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_onDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig_onDur = {
    DataAttributeModelType,
    "onDur",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_pulseConfig,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_LocSta_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_LocSta,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_Check,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_origin,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel = {
    DataAttributeModelType,
    "Cancel",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_blkEna,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_T,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_ctlNum,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_ctlVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_operTm,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_operTm = {
    DataAttributeModelType,
    "operTm",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_Cancel_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_ctlNum,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_opOk,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_opOk = {
    DataAttributeModelType,
    "opOk",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_opRcvd,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_opRcvd = {
    DataAttributeModelType,
    "opRcvd",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_operTimeout,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_operTimeout = {
    DataAttributeModelType,
    "operTimeout",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_q,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_origin,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_Mod_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_ldNs,
    0
};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_ldNs = {
    DataAttributeModelType,
    "ldNs",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_configRev,
    NULL,
    0,
    IEC61850_FC_EX,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_configRev = {
    DataAttributeModelType,
    "configRev",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_paramRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_paramRev = {
    DataAttributeModelType,
    "paramRev",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_valRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_valRev = {
    DataAttributeModelType,
    "valRev",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt_vendor,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_LDMODEXPF_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_LDMODEXPF_IHMI0 = {
    LogicalNodeModelType,
    "IHMI0",
    (ModelNode*) &iedModel_LDMODEXPF,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
};

DataObject iedModel_LDMODEXPF_IHMI0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_blkEna,
    0
};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_IHMI0_Loc = {
    DataObjectModelType,
    "Loc",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_subVal,
    0
};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_blkEna,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_q,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_Loc_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_Loc,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_LDMODEXPF_IHMI0_LocSta = {
    DataObjectModelType,
    "LocSta",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0,
    NULL,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_ctlModel,
    0
};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_blkEna,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_blkEna = {
    DataAttributeModelType,
    "blkEna",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_d,
    NULL,
    0,
    IEC61850_FC_BL,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig = {
    DataAttributeModelType,
    "pulseConfig",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_q,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_cmdQual,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_cmdQual = {
    DataAttributeModelType,
    "cmdQual",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_numPls,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_numPls = {
    DataAttributeModelType,
    "numPls",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_offDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_offDur = {
    DataAttributeModelType,
    "offDur",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_onDur,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig_onDur = {
    DataAttributeModelType,
    "onDur",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_pulseConfig,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT32U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_subEna,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_subEna = {
    DataAttributeModelType,
    "subEna",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_subID,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_subID = {
    DataAttributeModelType,
    "subID",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_subQ,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_VISIBLE_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_subQ = {
    DataAttributeModelType,
    "subQ",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_subVal,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_QUALITY,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_subVal = {
    DataAttributeModelType,
    "subVal",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_t,
    NULL,
    0,
    IEC61850_FC_SV,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta_tOpOk,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_LDMODEXPF_IHMI0_LocSta_tOpOk = {
    DataAttributeModelType,
    "tOpOk",
    (ModelNode*) &iedModel_LDMODEXPF_IHMI0_LocSta,
    NULL,
    NULL,
    0,
    IEC61850_FC_OR,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report0;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report1;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report2;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report3;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report4;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report5;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report6;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report7;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report8;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report9;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report10;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report11;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report12;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report13;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report14;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report15;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report16;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report17;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report18;
extern ReportControlBlock iedModel_LDMODEXPS_LLN0_report19;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report0;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report1;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report2;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report3;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report4;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report5;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report6;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report7;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report8;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report9;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report10;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report11;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report12;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report13;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report14;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report15;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report16;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report17;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report18;
extern ReportControlBlock iedModel_LDMODEXPF_LLN0_report19;

ReportControlBlock iedModel_LDMODEXPS_LLN0_report0 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT01", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report1};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report1 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT02", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report2};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report2 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT03", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report3};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report3 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT04", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report4};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report4 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT05", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report5};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report5 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT06", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report6};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report6 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT07", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report7};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report7 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT08", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report8};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report8 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT09", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report9};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report9 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT10", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report10};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report10 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT01", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report11};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report11 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT02", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report12};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report12 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT03", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report13};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report13 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT04", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report14};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report14 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT05", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report15};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report15 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT06", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report16};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report16 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT07", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report17};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report17 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT08", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report18};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report18 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT09", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPS_LLN0_report19};
ReportControlBlock iedModel_LDMODEXPS_LLN0_report19 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT10", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report0};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report0 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT01", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report1};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report1 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT02", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report2};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report2 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT03", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report3};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report3 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT04", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report4};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report4 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT05", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report5};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report5 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT06", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report6};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report6 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT07", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report7};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report7 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT08", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report8};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report8 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT09", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report9};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report9 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_CYC_EXT10", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_CYC_EXT", true, "RTE_LLN0_DS_RPT_CYC_EXT", 1, 24, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report10};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report10 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT01", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report11};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report11 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT02", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report12};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report12 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT03", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report13};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report13 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT04", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report14};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report14 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT05", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report15};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report15 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT06", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report16};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report16 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT07", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report17};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report17 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT08", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report18};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report18 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT09", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_LDMODEXPF_LLN0_report19};
ReportControlBlock iedModel_LDMODEXPF_LLN0_report19 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_RPT_DQCHG_EXT10", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_RPT_DQCHG_EXT", true, "RTE_LLN0_DS_RPT_DQCHG_EXT", 1, 27, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, NULL};


extern GSEControlBlock iedModel_LDMODEXPS_LLN0_gse0;
extern GSEControlBlock iedModel_LDMODEXPS_LLN0_gse1;
extern GSEControlBlock iedModel_LDMODEXPF_LLN0_gse0;

static PhyComAddress iedModel_LDMODEXPS_LLN0_gse0_address = {
  4,
  1536,
  12288,
  {0x1, 0xc, 0xcd, 0x1, 0x0, 0x5}
};

GSEControlBlock iedModel_LDMODEXPS_LLN0_gse0 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_GSE_EXT", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_GSE_EXT", "RTE_LLN0_GSE_EXT", 1, false, &iedModel_LDMODEXPS_LLN0_gse0_address, -1, -1, &iedModel_LDMODEXPS_LLN0_gse1};

static PhyComAddress iedModel_LDMODEXPS_LLN0_gse1_address = {
  4,
  261,
  12288,
  {0x1, 0xc, 0xcd, 0x1, 0x0, 0x3}
};

GSEControlBlock iedModel_LDMODEXPS_LLN0_gse1 = {&iedModel_LDMODEXPS_LLN0, "RTE_LLN0_CB_GSE_INT", "XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_GSE_INT", "RTE_LLN0_GSE_INT", 1, false, &iedModel_LDMODEXPS_LLN0_gse1_address, -1, -1, &iedModel_LDMODEXPF_LLN0_gse0};
GSEControlBlock iedModel_LDMODEXPF_LLN0_gse0 = {&iedModel_LDMODEXPF_LLN0, "RTE_LLN0_CB_GSE_EXT", "XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_GSE_EXT", "RTE_LLN0_GSE_EXT", 1, false, NULL, -1, -1, NULL};





IedModel iedModel = {
    "AUT1A_SITE_1",
    &iedModel_LDMODEXPS,
    &iedModelds_LDMODEXPS_LLN0_RTE_LLN0_DS_RPT_DQCHG_EXT,
    &iedModel_LDMODEXPS_LLN0_report0,
    &iedModel_LDMODEXPS_LLN0_gse0,
    NULL,
    NULL,
    NULL,
    NULL,
    initializeValues
};

static void
initializeValues()
{

iedModel_LDMODEXPS_LLN0_Beh_d.mmsValue = MmsValue_newVisibleString("Ldevice current status");

iedModel_LDMODEXPS_LLN0_Health_d.mmsValue = MmsValue_newVisibleString("Health of the function");

iedModel_LDMODEXPS_LLN0_InRef1_purpose.mmsValue = MmsValue_newVisibleString("DYN_LDMODEXPS_TC fictive_1_Dbpos");

iedModel_LDMODEXPS_LLN0_InRef1_setSrcRef.mmsValue = MmsValue_newVisibleString("XX_AUT1A_SITE_1_LDTGINFRA_1/GAPC3.DPCSO1");

iedModel_LDMODEXPS_LLN0_InRef1_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPS_LLN0_InRef2_purpose.mmsValue = MmsValue_newVisibleString("DYN_LDMODEXPS_Mode exploitation tranche_1_BOOLEAN");

iedModel_LDMODEXPS_LLN0_InRef2_setSrcCB.mmsValue = MmsValue_newVisibleString("XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.RTE_LLN0_CB_GSE_EXT");

iedModel_LDMODEXPS_LLN0_InRef2_setSrcRef.mmsValue = MmsValue_newVisibleString("XX_BCU_4ZBRAN_1_LDMODEXPF_1/LLN0.LocSta");

iedModel_LDMODEXPS_LLN0_InRef2_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPS_LLN0_LocSta_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(3);

iedModel_LDMODEXPS_LLN0_LocSta_d.mmsValue = MmsValue_newVisibleString("Command_signal for Substation in local_remote mode");

iedModel_LDMODEXPS_LLN0_Mod_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(3);

iedModel_LDMODEXPS_LLN0_Mod_d.mmsValue = MmsValue_newVisibleString("Activation_deactivation command");

iedModel_LDMODEXPS_LLN0_Mod_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_LDMODEXPS_LLN0_NamPlt_ldNs.mmsValue = MmsValue_newVisibleString("IEC 61850-7-4:2007B");

iedModel_LDMODEXPS_LLN0_NamPlt_configRev.mmsValue = MmsValue_newVisibleString("01.00.000");

iedModel_LDMODEXPS_LLN0_NamPlt_d.mmsValue = MmsValue_newVisibleString("Name plate of the logical node");

iedModel_LDMODEXPS_LLN0_NamPlt_swRev.mmsValue = MmsValue_newVisibleString("01.00.000");

iedModel_LDMODEXPS_GAPC0_Beh_d.mmsValue = MmsValue_newVisibleString("Current LN status");

iedModel_LDMODEXPS_GAPC0_Ind1_d.mmsValue = MmsValue_newVisibleString("One or more bay in local mode");

iedModel_LDMODEXPS_GAPC0_SPCSO1_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(3);

iedModel_LDMODEXPS_GAPC0_SPCSO1_d.mmsValue = MmsValue_newVisibleString("Substation in backup alarm mode AS");

iedModel_LDMODEXPS_GAPC0_SPCSO2_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(3);

iedModel_LDMODEXPS_GAPC0_SPCSO2_d.mmsValue = MmsValue_newVisibleString("Substation in remote alarm mode TA");

iedModel_LDMODEXPF_LLN0_Beh_d.mmsValue = MmsValue_newVisibleString("Ldevice current status");

iedModel_LDMODEXPF_LLN0_Health_d.mmsValue = MmsValue_newVisibleString("Health of the function");

iedModel_LDMODEXPF_LLN0_InRef1_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef1_setSrcCB.mmsValue = MmsValue_newVisibleString("XX_AUT1A_SITE_1_LDMODEXPS_1/LLN0.RTE_LLN0_CB_GSE_EXT");

iedModel_LDMODEXPF_LLN0_InRef1_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef10_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef10_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef2_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef2_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef3_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef3_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef4_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef4_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef5_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef5_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef6_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef6_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef7_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef7_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef8_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef8_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_InRef9_intAddr.mmsValue = MmsValue_newVisibleString("TBD");

iedModel_LDMODEXPF_LLN0_InRef9_tstEna.mmsValue = MmsValue_newBoolean(false);

iedModel_LDMODEXPF_LLN0_LocSta_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(3);

iedModel_LDMODEXPF_LLN0_LocSta_d.mmsValue = MmsValue_newVisibleString("Command_signal bay in local_remote mode");

iedModel_LDMODEXPF_LLN0_Mod_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(3);

iedModel_LDMODEXPF_LLN0_Mod_d.mmsValue = MmsValue_newVisibleString("Activation_deactivation command");

iedModel_LDMODEXPF_LLN0_Mod_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_LDMODEXPF_LLN0_NamPlt_ldNs.mmsValue = MmsValue_newVisibleString("IEC 61850-7-4:2007B");

iedModel_LDMODEXPF_LLN0_NamPlt_configRev.mmsValue = MmsValue_newVisibleString("01.00.000");

iedModel_LDMODEXPF_LLN0_NamPlt_d.mmsValue = MmsValue_newVisibleString("Name plate of the logical node");

iedModel_LDMODEXPF_LLN0_NamPlt_swRev.mmsValue = MmsValue_newVisibleString("01.00.000");

iedModel_LDMODEXPF_IHMI0_Beh_d.mmsValue = MmsValue_newVisibleString("Current LN status");

iedModel_LDMODEXPF_IHMI0_LocSta_ctlModel.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_LDMODEXPF_IHMI0_LocSta_d.mmsValue = MmsValue_newVisibleString("LocSta to be used by LD of the functional bay and GW resulting from Site mode and bay mode");
}
