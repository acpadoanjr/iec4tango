#ifndef IecClientTask_H
#define IecClientTask_H

#include <tango.h>

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>

#include <yat/CommonHeader.h>
#include <yat/threading/Message.h>
#include <yat/any/GenericContainer.h>
#include <yat/memory/DataBuffer.h>
#include <yat/memory/Allocator.h>
#include <yat4tango/DynamicAttributeManager.h>
#include <yat4tango/DeviceTask.h>

extern "C" {
#include <libiec61850/iec61850_client.h>
#include <libiec61850/iec61850_common.h>
#include <libiec61850/hal_thread.h>
}
#include "IecDataPointClass.h"

namespace IecClient_ns
{



#define OPER_MSG_TYPE (yat::FIRST_USER_MSG+1)
#define POLL_MSG_TYPE (yat::FIRST_USER_MSG+2)
#define INIT_MSG_TYPE (yat::FIRST_USER_MSG+3)


        struct iecVariable {
                string da_name;
                string parent_name;
                string FC;
                MmsType type;
                MmsVariableSpecification* var_spec;
                MmsVariableSpecification* parent_var_spec;
                int index_from_parent;
                bool isStructure = false;
                bool isArray = false;
                int structureSize = -1;
                bool hasQuality = false;
                bool isQuality = false;
                bool isDO = false;
                bool isVar = false;
                bool isTimestamp = false;
                int QualityIndex = -1;
                int TimestampIndex = -1;
                int nextDOIndex = -1;
                int dataSetDirectoryIndex = -1;
                bool isMapped = false;
                IecDataPoint *associatedDataPoint = NULL;
        };



        class IecClientTask : public yat4tango::DeviceTask
        {
        public:
                IecClientTask (Tango::DeviceImpl * host_device);

                virtual ~IecClientTask ();
                static void reportCallbackFunction(void* parameter, ClientReport report);
                void start_iec()
                        throw (Tango::DevFailed);
                virtual void exit ()
                        throw (Tango::DevFailed);

                void set_tg (Tango::Util *tangoUtil);
                void set_mappedDataPoints(vector<IecDataPoint*> vectorOfMappedDP);
                bool isConnected;
                void set_IPAddress(string IEDaddress, Tango::DevShort IEDport);
                void set_ReportName(string ReportName1, string ReportName2);
                void set_ReportNames(std::vector<string> iBufRepNames, std::vector<string> iUnBufRepNames);
                void invalidate();
                bool startControls();
                IecDataPoint* GetCachedIecDataPoint();





        protected:
                //! the actual yat::Message handler (must be implemented by derived classes)
                //! be sure that your implementation only throws Tango::DevFailed exceptions
                virtual void process_message (yat::Message& msg);

        private:
                IedClientError clerror;
                IedConnection con;
                LinkedList dataSetDirectory;  // default iec61850lib list of dataset elements
                //LinkedList nonDataSetDirectory; // not sure to be used
                //LinkedList dataSetVariables;
                ClientDataSet clientDataSet = NULL;
                //ClientReportControlBlock rcb = NULL;
                std::vector<ClientDataSet> clientDataSets;
                std::vector<string> dataSetNames;
                std::vector<ClientReportControlBlock*> rcbs;
                std::vector<string> bufRepNames;
                std::vector<string> unBufRepNames;

                Tango::Util *tg;
                string ipaddress;
                Tango::DevShort port;
                string dataSetName1 = ""; //"BCULDDJ/LLN0.DS00";
                string reportName1 = "BCULDDJ/LLN0.RP.urcb01";
                string dataSetName2 = "";
                string reportName2 = "";

                /*
                 * Memory cache to create dynamically DataPoints
                 * that will be used to communicate iec variable
                 * changes from reports. This is useful in case of an avalanche.
                 */
                yat::CachedAllocator<IecDataPoint, yat::Mutex>  *CachedIecDataPoints;

                /*
                 * Vector with all DataPoints which are configurated in Tango DB(All LNs).
                 */
                vector<IecDataPoint*> mappedDataPoints;

                /*
                 * list of Variables belonging to Data Set
                 * (scruture element is useful to algorithm).
                 * It is created dynamically
                 */
                std::vector<iecVariable>  listDataSetVariable;

                /*
                 * list of Variables that are mapped but not in
                 * DataSet (no reporting assicated!) // TODO: implement it.
                 */
                std::vector<iecVariable>  listNonIecVariable; //

                /*
                 * list of all attributes found on IED.
                 * Build dinamically. // TODO: implement it.
                 */
                std::vector<iecVariable> listAllVariables;

                std::vector<string> listLogicalDevices;
                std::vector<string> listLogicalNodes;
                std::vector<string> listDataObjects;




///////////////////////////////////////////////////////////////////////////////
//                                  Methods                                  //
///////////////////////////////////////////////////////////////////////////////


                bool start_iecConnection();
                bool start_iecDataSet();
                bool start_iecReport();
                bool start_iecModel();

                void fillListOfVariables(vector<iecVariable> &list_da,
                                         const char* directory_name,
                                         FunctionalConstraint FC,
                                         MmsVariableSpecification* parent_var_spec);

                int mapDS2DP(vector<MmsValue*> list_mms,
                             Timestamp* stdTimestamp,
                             ClientReport rep,
                             int i);

                void serializeMmsValue(vector<MmsValue*> &list_mms,
                                       MmsValue* mmsvalue);
                bool isConnectedf();
                void closeConnection();
                MmsVariableSpecification* get_DO_varSpec_by_FC(string DOnameWithFC);

                bool fill_list_of_DataSet(vector<iecVariable> &list_to_be_filled,
                                          LinkedList DataDirectory);

                void organize_q_t_in_list(vector<iecVariable> &list_to_be_org);
                bool read_mmsValue(IecDataPoint *idp);

        };
}
#endif
