/*----- PROTECTED REGION ID(CSWI.h) ENABLED START -----*/
//=============================================================================
//
// file :        CSWI.h
//
// description : Include file for the CSWI class
//
// project :     iec4tango
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef CSWI_H
#define CSWI_H

#include <tango.h>
#include "../CommonLN/CommonLN.h"
#include "../IecClient.h"



/*----- PROTECTED REGION END -----*/	//	CSWI.h

/**
 *  CSWI class description:
 *    
 */

namespace CSWI_ns
{
/*----- PROTECTED REGION ID(CSWI::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	CSWI::Additional Class Declarations

class CSWI : public CommonLN_ns::CommonLN
{

/*----- PROTECTED REGION ID(CSWI::Data Members) ENABLED START -----*/

//	Add your own data members
        //	iecMapping:	Mapping between IEC and Tango.
public:
        IecClient_ns::IecDataPoint  *dp_Pos_read=NULL;
        IecClient_ns::IecDataPoint  *dp_Local_read=NULL;
        IecClient_ns::IecDataPoint  *dp_open=NULL;
        IecClient_ns::IecDataPoint  *dp_close=NULL;
        bool has_Pos_pointer = false;
        bool has_Local_pointer = false;


/*----- PROTECTED REGION END -----*/	//	CSWI::Data Members

//	Device property data members
public:
	//	iecMapping:	Mapping between IEC and Tango.
	vector<string>	iecMapping;

//	Attribute data members
public:
	Tango::DevShort	*attr_Pos_read;
	Tango::DevBoolean	*attr_Local_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	CSWI(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	CSWI(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	CSWI(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~CSWI() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : CSWI::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute Pos related methods
 *	Description: 
 *
 *	Data type:	Tango::DevShort
 *	Attr type:	Scalar
 */
	virtual void read_Pos(Tango::Attribute &attr);
	virtual bool is_Pos_allowed(Tango::AttReqType type);
/**
 *	Attribute Local related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Local(Tango::Attribute &attr);
	virtual bool is_Local_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : CSWI::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command open related method
	 *	Description: 
	 *
	 */
	virtual void open();
	virtual bool is_open_allowed(const CORBA::Any &any);
	/**
	 *	Command close related method
	 *	Description: 
	 *
	 */
	virtual void close();
	virtual bool is_close_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : CSWI::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(CSWI::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes
        void linkIECmapping_pointer();


/*----- PROTECTED REGION END -----*/	//	CSWI::Additional Method prototypes
};

/*----- PROTECTED REGION ID(CSWI::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	CSWI::Additional Classes Definitions

}	//	End of namespace

#endif   //	CSWI_H
