#include "IecDataPointClass.h"

namespace IecClient_ns
{

        IecDataPoint::IecDataPoint()
        {
                /*To be used to avoid runtime errors*/
                this->tangoDataPoint.ctlValBoolean = new Tango::DevBoolean[1];
                this->tangoDataPoint.valueShort = new Tango::DevShort[1];
                this->tangoDataPoint.valueUShort = new Tango::DevUShort[1];
                this->tangoDataPoint.valueBoolean = new Tango::DevBoolean[1];
                this->tangoDataPoint.valueDouble = new Tango::DevDouble[1];
                this->tangoDataPoint.valueLong = new Tango::DevLong[1];
                this->tangoDataPoint.valueULong = new Tango::DevULong[1];
                this->tangoDataPoint.valueString = new Tango::DevString[1];
                *this->tangoDataPoint.ctlValBoolean = false;
                *this->tangoDataPoint.valueShort = 0;
                *this->tangoDataPoint.valueUShort = 0;
                *this->tangoDataPoint.valueBoolean = false;
                *this->tangoDataPoint.valueDouble = 0.0;
                *this->tangoDataPoint.valueLong = 0;
                *this->tangoDataPoint.valueULong = 0;

        }

        IecDataPoint::~IecDataPoint()
        {
                if (this->iecDataPoint.value != NULL)
                        MmsValue_delete(this->iecDataPoint.value);
                if (this->tangoDataPoint.valueShort != NULL)
                        delete this->tangoDataPoint.valueShort;
                if (this->tangoDataPoint.valueBoolean != NULL)
                        delete this->tangoDataPoint.valueBoolean;
                if (this->tangoDataPoint.valueDouble != NULL)
                        delete this->tangoDataPoint.valueDouble;
                if (this->tangoDataPoint.valueLong != NULL)
                        delete this->tangoDataPoint.valueLong;
                if (this->tangoDataPoint.valueString != NULL)
                        delete this->tangoDataPoint.valueString;
                if (this->tangoDataPoint.ctlValBoolean != NULL)
                        delete this->tangoDataPoint.ctlValBoolean;
        }


        bool IecDataPoint::define_IecDataPoint(string dataAttributeName)
        {
                static regex reg_da("^(\\w+)/(\\w+)\\.(\\w+)(?:\\.([\\w|\\.]*))?\\[(\\w\\w)\\]$");
                static regex reg_ln("([A-Z|0]{4})(\\d+)?$");

                smatch match_da;
                smatch match_ln;

                if (regex_search(dataAttributeName, match_da, reg_da) == true)
                {
                        logicalDevice = match_da.str(1);
                        logicalNode = match_da.str(2);
                        if (regex_search(logicalNode, match_ln, reg_ln) == true)
                        {
                                logicalNodeType = match_ln.str(1);
                        }
                        else
                        {
                                cout << "Problem reading LN Class while defining IecDataPoint." << endl;
                                return false;
                        }


                        dataAttribute = match_da.str(4); // or "sdo.da"
                        dataObject = match_da.str(3);

                        FC = match_da.str(5);
                        if(FC.compare("CO")==0)
                                isControl = true;
                        return true;
                }

                cout << "No match while defining IecDataPoint." << endl;
                return false;

        }

        bool IecDataPoint::define_IecDataPoint(string iecMapping, string deviceName)
        {
                static regex reg_do("^(\\w+):(\\w+)(?:.([\\w\\.]+))?\\[(\\w\\w)\\]:(\\w+)$");
                static regex reg_dev("^(\\w+)/(\\w{4})/(\\w+)$");
                static regex reg_dev_do("^(\\w+)/(\\w{3})/((?:[a-z]|[A-Z])+\\d+)(\\w+)$");
                static regex reg_ln("([A-Z|0]{4})(\\d+)?$");


                smatch match_do;
                smatch match_dev;
                smatch match_dev_do;
                smatch match_ln;

                if (regex_search(deviceName,match_dev,reg_dev)==true){ // case the device represents a ln:  the regular case
                        logicalDevice = match_dev.str(1);
                        logicalNodeType = match_dev.str(2);
                        logicalNode = match_dev.str(3);

                        if (regex_search(iecMapping,match_do,reg_do)==true){
                                TangoAttributeName = match_do.str(1);
                                dataObject = match_do.str(2);
                                dataAttribute = match_do.str(3);
                                FC = match_do.str(4);
                                if(FC.compare("CO")==0)
                                        isControl = true;
                                CDC = match_do.str(5);
                                return true;
                        }
                        else{
                                cout << "Problem defining IecDataPoint." << endl;
                                return false;
                        }
                }
                else if (regex_search(deviceName,match_dev_do,reg_dev_do)==true){  // case the device represents a do : the exception
                        logicalDevice = match_dev_do.str(1);
                        //cout << "testing ld: " << logicalDevice << endl;
                        logicalNode = match_dev_do.str(3);
                        //cout << "testing ln: " << logicalNode << endl;
                        dataObject = match_dev_do.str(4);
                        if (regex_search(logicalNode, match_ln, reg_ln) == true)
                        {
                                logicalNodeType = match_ln.str(1);
                                //cout << "testing lnt: " << logicalNodeType << endl;

                        }
                        else
                        {
                                cout << "Problem reading LN Class while defining IecDataPoint." << endl;
                                return false;
                        }
                        if (regex_search(iecMapping,match_do,reg_do)==true){
                                TangoAttributeName = match_do.str(1);
                                string possible_dataObject(match_do.str(2));
                                string possible_Attribute(match_do.str(3));

                                //cout << "Possibles do et da: " << possible_dataObject << " et " << possible_Attribute << endl;

                                if (!possible_dataObject.compare("Beh") || !possible_dataObject.compare("Health")){
                                        dataObject = possible_dataObject;
                                        dataAttribute = possible_Attribute;

                                } else {
                                        dataAttribute = possible_dataObject+ "." + possible_Attribute;
                                }
                                //cout << "testing do: " << dataObject << endl;
                                //cout << "testing da: " << dataAttribute << endl;
                                FC = match_do.str(4);
                                if(FC.compare("CO")==0)
                                        isControl = true;
                                CDC = match_do.str(5);
                                //cout << "testing CDC: " << CDC << endl;
                                //cout << "testing FC: " << FC << endl;

                                return true;
                        }
                }
                else{
                        cout << "Not LN or DO. Problem defining IecDataPoint." << endl;
                        return false;
                }

                cout << "Problem defining IecDataPoint." << endl;
                return false;
        }


        FunctionalConstraint IecDataPoint::get_FC()
        {
                return FunctionalConstraint_fromString(FC.c_str());
        }

        string IecDataPoint::get_FC_string()
        {
                return this->FC;
        }


        string IecDataPoint::get_iecAddress(bool withFC)
        {
                string addr;
                addr.append(logicalDevice);
                addr.append("/");
                addr.append(logicalNode);
                addr.append(".");
                addr.append(dataObject);
                if (!(dataAttribute.empty())){
                        addr.append(".");
                        addr.append(dataAttribute);
                }
                if(withFC){
                        addr.append("[");
                        addr.append(FC);
                        addr.append("]");
                }
                return addr;
        }


        void IecDataPoint::set_timestamp_now(){
                Timestamp ts;
                struct timeval tp;
                gettimeofday(&tp, NULL);
                long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
                Timestamp_setTimeInMilliseconds(&ts, ms);
                this->set_iecT(ts);
        }

        string IecDataPoint::get_DOAddress(bool withFC){
                string addr;
                addr.append(logicalDevice);
                addr.append("/");
                addr.append(logicalNode);
                addr.append(".");
                addr.append(dataObject);
                if(withFC){
                        addr.append("[");
                        addr.append(FC);
                        addr.append("]");
                }
                return addr;
        }

        string IecDataPoint::get_dataobject(){
                return dataObject;
        }

        string IecDataPoint::get_tangoAddress()
        {
                string addr;
                addr.append(logicalDevice);
                addr.append("/");
                addr.append(logicalNodeType);
                addr.append("/");
                addr.append(logicalNode);
                addr.append(".");
                addr.append(TangoAttributeName);
                return addr;
        }

        bool IecDataPoint::compareAttributeReferenceToThis(IecDataPoint* other)
        {
                //printf("da %s X %s; do %s X %s \n", dataAttribute.c_str(), other->dataAttribute.c_str(), dataObject.c_str(), other->dataObject.c_str());
                if (!dataAttribute.compare(other->dataAttribute)==0)
                        return false;
                if (!dataObject.compare(other->dataObject)==0)
                        return false;
                if (!FC.compare(other->FC)==0)
                        return false;
                if (!logicalNode.compare(other->logicalNode)==0)
                        return false;
                if (!logicalDevice.compare(other->logicalDevice)==0)
                        return false;
                return true;
        }

        IecDataPoint* IecDataPoint::findThisReferenceInVector(vector<IecDataPoint*> vectorWhereToFind, bool createIfNull)
        {
                for (std::vector<IecDataPoint*>::iterator it=vectorWhereToFind.begin();
                     it != vectorWhereToFind.end(); ++it){
                        IecDataPoint *dp = (*it);
                        if (compareAttributeReferenceToThis(dp))
                                return dp;
                }
                if(createIfNull){
                        IecDataPoint* newdp = new IecDataPoint();
                        if(newdp->define_IecDataPoint(this->get_iecAddress(true)))
                                vectorWhereToFind.push_back(newdp);
                }
                //cout << "No reference found in vector " << get_tangoAddress() << endl;;
                return NULL;
        }

        void IecDataPoint::convert()
        {
                Quality q = this->iecDataPoint.quality;
                Validity v =  Quality_getValidity(&q);
                if (v != 0)
                        this->tangoDataPoint.quality = Tango::ATTR_INVALID;
                else
                        this->tangoDataPoint.quality = Tango::ATTR_VALID;
                time_t ts =(time_t)Timestamp_getTimeInSeconds(&this->iecDataPoint.timestamp);
                uint64_t tm = (Timestamp_getTimeInMs(&this->iecDataPoint.timestamp)) % 1000;
                suseconds_t tms = (suseconds_t) (tm * 1000);
                this->tangoDataPoint.timestamp.tv_sec = ts;
                this->tangoDataPoint.timestamp.tv_usec = tms;
                if(this->iecDataPoint.value == NULL && !this->isControl){
                        this->tangoDataPoint.quality = Tango::ATTR_INVALID;
                        return;
                }
                if((this->CDC.compare("DPC")==0) || (this->CDC.compare("ENS")==0) || (this->CDC.compare("ENC")==0)){
                        int statein = 3;
                        if(this->CDC.compare("DPC")==0){
                                if(!isControl){
                                        statein = (int) Dbpos_fromMmsValue(this->iecDataPoint.value);
                                }
                                //------------------  ctlVal -------------------------
                                if(isControl){
                                        if(this->iecDataPoint.ctlVal == NULL){
                                                this->iecDataPoint.ctlVal = MmsValue_newBoolean(false);
                                        }
                                        if(this->tangoDataPoint.ctlValBoolean != NULL){
                                                bool b = *this->tangoDataPoint.ctlValBoolean;
                                                MmsValue_setBoolean(this->iecDataPoint.ctlVal,b);

                                        }
                                        else{
                                                this->tangoDataPoint.ctlValBoolean = new Tango::DevBoolean(false);
                                        }
                                }
                                //------------------  ctlVal -------------------------

                        }
                        else
                                statein = MmsValue_toUint32(this->iecDataPoint.value);

                        Tango::DevShort state = statein;
                        *this->tangoDataPoint.valueShort = state;
                        if(!isControl){
                                this->device->push_archive_event(this->TangoAttributeName,
                                                                 this->tangoDataPoint.valueShort,
                                                                 this->tangoDataPoint.timestamp,
                                                                 this->tangoDataPoint.quality, 1 , 0, false);
                        }

                } else if ((this->CDC.compare("SPS")==0) || (this->CDC.compare("SPC")==0))
                {
                        bool statein = MmsValue_getBoolean(this->iecDataPoint.value);
                        Tango::DevBoolean state = statein;
                        *this->tangoDataPoint.valueBoolean = state;

                        this->device->push_archive_event(this->TangoAttributeName,
                                                         this->tangoDataPoint.valueBoolean,
                                                         this->tangoDataPoint.timestamp,
                                                         this->tangoDataPoint.quality, 1 , 0, false);
                 }
                else if ((this->CDC.compare("MV")==0) || (this->CDC.compare("CMV")==0))
                {
                        double statein = MmsValue_toDouble(this->iecDataPoint.value);
                        Tango::DevDouble state = statein;
                        *this->tangoDataPoint.valueDouble = state;

                        this->device->push_archive_event(this->TangoAttributeName,
                                                         this->tangoDataPoint.valueDouble,
                                                         this->tangoDataPoint.timestamp,
                                                         this->tangoDataPoint.quality, 1 , 0, false);
                }
        }

        //No use....
        void IecDataPoint::changeCtlNum(){
                ctlNum++;
        }

        IecTangoMap IecDataPoint::mapOfTypes(){
                static bool once = false;
                static IecTangoMap map;

                if(!once){
                        map[MMS_ARRAY] = "None";
                        map[MMS_STRUCTURE] = "None";
                        map[MMS_BOOLEAN] = "DevBoolean";
                        map[MMS_BIT_STRING] = "DevShort";
                        map[MMS_INTEGER] = "DevLong";
                        map[MMS_UNSIGNED] = "DevULong";
                        map[MMS_FLOAT] = "DevDouble";
                        map[MMS_OCTET_STRING] = "DevString";
                        map[MMS_VISIBLE_STRING] = "DevString";
                        map[MMS_GENERALIZED_TIME] = "None";
                        map[MMS_BINARY_TIME] = "None";
                        map[MMS_BCD] = "None";
                        map[MMS_OBJ_ID] = "None";
                        map[MMS_STRING] = "DevString";
                        map[MMS_UTC_TIME] = "Timestamp";
                        map[MMS_DATA_ACCESS_ERROR] = "None";
                        once = true;
                }
                return map;
        }

        void IecDataPoint::set_iecValue(MmsValue* value){
                if (!tangoDataPoint.hasTangoType){
                        IecTangoMap mp = mapOfTypes();
                        MmsType mt = MmsValue_getType(value);
                        tangoDataPoint.TangoType = mp[mt];
                        tangoDataPoint.hasTangoType = true;
                }
                iecDataPoint.value = value;
        }
        void IecDataPoint::set_iecQ(Quality quality){
                iecDataPoint.quality = quality;
        }
        void IecDataPoint::set_iecT(Timestamp timestamp){
                iecDataPoint.timestamp = timestamp;
        }
        void IecDataPoint::set_iecCtlVal(MmsValue* ctlVal){
                if (!tangoDataPoint.hasTangoType){
                        IecTangoMap mp = mapOfTypes();
                        MmsType mt = MmsValue_getType(ctlVal);
                        tangoDataPoint.TangoType = mp[mt];
                        tangoDataPoint.hasTangoType = true;
                }
                iecDataPoint.ctlVal = ctlVal;
        }

        MmsValue* IecDataPoint::get_iecValue(){
                return iecDataPoint.value;
        }
        Quality IecDataPoint::get_iecQ(){
                return iecDataPoint.quality;
        }
        Timestamp IecDataPoint::get_iecT(){
                return iecDataPoint.timestamp;
        }
        MmsValue* IecDataPoint::get_iecCtlVal(){
                return iecDataPoint.ctlVal;
        }
        bool IecDataPoint::hasValue(){
                if(iecDataPoint.value == NULL)
                        return false;
                else
                        return true;
        }
        void IecDataPoint::invalidate(){
                Validity v = QUALITY_VALIDITY_INVALID;
                Quality_setValidity(&iecDataPoint.quality,v);
                set_timestamp_now();
                convert();
        }
        void IecDataPoint::validate(){
                Validity v = QUALITY_VALIDITY_GOOD;
                Quality_setValidity(&iecDataPoint.quality,v);
                set_timestamp_now();
                convert();
        }


}



///////////////////////////////////////////////////////////////////////////////
//                       Some types defined externally                       //
///////////////////////////////////////////////////////////////////////////////

// typedef DevBoolean (DeviceImpl::*Bo_CmdMethPtr)();
// typedef DevShort (DeviceImpl::*Sh_CmdMethPtr)();
// typedef DevLong (DeviceImpl::*Lg_CmdMethPtr)();
// typedef DevFloat (DeviceImpl::*Fl_CmdMethPtr)();
// typedef DevDouble (DeviceImpl::*Db_CmdMethPtr)();
// typedef DevUShort (DeviceImpl::*US_CmdMethPtr)();
// typedef DevULong (DeviceImpl::*UL_CmdMethPtr)();
// typedef DevString (DeviceImpl::*Str_CmdMethPtr)();

//  MmsType:
// /*! this represents all MMS array types (arrays contain uniform elements) */
// MMS_ARRAY = 0,
// /*! this represents all complex MMS types (structures) */
//         MMS_STRUCTURE = 1,
// /*! boolean value */
//         MMS_BOOLEAN = 2,
// /*! bit string */
//         MMS_BIT_STRING = 3,
// /*! represents all signed integer types */
//         MMS_INTEGER = 4,
// /*! represents all unsigned integer types */
//         MMS_UNSIGNED = 5,
// /*! represents all float type (32 and 64 bit) */
//         MMS_FLOAT = 6,
// /*! octet string (unstructured bytes) */
//         MMS_OCTET_STRING = 7,
// /*! MMS visible string */
//         MMS_VISIBLE_STRING = 8,
//         MMS_GENERALIZED_TIME = 9,
//         MMS_BINARY_TIME = 10,
//         MMS_BCD = 11,
//         MMS_OBJ_ID = 12,
// /*! MMS unicode string */
//         MMS_STRING = 13,
// /*! MMS UTC time type */
//         MMS_UTC_TIME = 14,
// /*! This represents an error code as returned by MMS read services */
//         MMS_DATA_ACCESS_ERROR = 15
