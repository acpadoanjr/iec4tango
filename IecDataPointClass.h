#ifndef IecDataPointClass_H
#define IecDataPointClass_H

#include <tango.h>
#include <signal.h>
#include <time.h>
#include <regex>


extern "C" {
#include <libiec61850/iec61850_common.h>
#include <libiec61850/iec61850_client.h>
#include <libiec61850/mms_common.h>
#include <libiec61850/mms_types.h>
#include <libiec61850/mms_value.h>
}


struct iecDataPointStruct {
        MmsValue *value = NULL;
        Timestamp timestamp;
        Quality quality;
        MmsValue *ctlVal = NULL;
};

struct tangoDataPointStruct {
        Tango::AttrQuality quality = Tango::ATTR_INVALID;
        timeval timestamp = (struct timeval){ 0 };
        Tango::DevBoolean  *ctlValBoolean = NULL;
        Tango::DevShort *valueShort = NULL;
        Tango::DevUShort *valueUShort = NULL;
        Tango::DevBoolean *valueBoolean = NULL;
        Tango::DevDouble *valueDouble = NULL;
        Tango::DevLong *valueLong = NULL;
        Tango::DevULong *valueULong = NULL;
        Tango::DevString *valueString = NULL;
        string TangoType = "None";
        bool hasTangoType = false;
};

typedef std::map<MmsType, string> IecTangoMap;



namespace IecClient_ns
{
        class IecDataPoint{
        public:
                IecDataPoint ();
                ~IecDataPoint ();

                bool define_IecDataPoint (string dataAttributeName);
                bool define_IecDataPoint (string iecMapping, string IecDataPointReference);
                FunctionalConstraint get_FC();
                string get_FC_string();
                string get_iecAddress(bool withFC=false);
                string get_DOAddress(bool withFC=false);
                string get_dataobject();
                void set_iecValue(MmsValue* value);
                void set_iecCtlVal(MmsValue* ctlVal);
                void set_iecQ(Quality quality);
                void set_iecT(Timestamp timestamp);
                MmsValue* get_iecValue();
                Quality get_iecQ();
                Timestamp get_iecT();
                MmsValue* get_iecCtlVal();
                bool hasValue();
                void invalidate();
                void validate();

                string get_tangoAddress();
                bool compareAttributeReferenceToThis(IecDataPoint* other);
                IecDataPoint* findThisReferenceInVector(vector<IecDataPoint*> vectorWhereToFind, bool createIfNull=true);
                void convert();
                tangoDataPointStruct tangoDataPoint;
                Tango::Attribute* attribute = NULL;
                Tango::DeviceImpl* device = NULL;
                bool Report=false;
                string ReportName; // Yet to be used
                string TangoAttributeName;
                ControlObjectClient controlObj;
                bool isControl= false;
                void changeCtlNum();
                void set_timestamp_now();
                static IecTangoMap mapOfTypes();

        private:
                iecDataPointStruct iecDataPoint;
                string logicalNode;
                string logicalNodeType;
                string logicalDevice;
                string dataObject;
                string dataAttribute;
                string FC;
                string CDC;
                int ctlNum = 0;
        };

}

#endif
